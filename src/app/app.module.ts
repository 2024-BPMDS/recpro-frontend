import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TranslocoModule} from "@ngneat/transloco";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {MaterialModule} from "./material/material.module";
import {TranslocoRootModule} from "./util/transloco-root/transloco-root.module";
import {ClarityModule} from "@clr/angular";
import {CustomClarityModule} from "./clarity/custom-clarity.module";
import {LayoutModule} from "./layout/layout.module";
import { UserComponent } from './components/user/user.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {RouterModule} from "@angular/router";
import {BpmModule} from "./components/bpm/bpm.module";
import {CommonModule} from "@angular/common";
import {UserInterceptor} from "./components/user/UserInterceptor";
import {FilterModule} from "./components/filter/filter.module";
import {FilterModelingModule} from "./components/filter/filter-modeling/filter-modeling.module";
import {RatingModule} from "./components/rating/rating.module";
import {RatingModelingModule} from "./components/rating/rating-modeling/rating-modeling.module";
import {RatingExecutionModule} from "./components/rating/rating-execution/rating-execution.module";
import { UserCreatorDialogComponent } from './components/user/user-creator-dialog/user-creator-dialog.component';
import {NgxSpinnerModule} from "ngx-spinner";


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    DashboardComponent,
    UserCreatorDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TranslocoRootModule,
    TranslocoModule,
    ClarityModule,
    CustomClarityModule,
    LayoutModule,
    RouterModule.forRoot([], {useHash: true}),
    BpmModule,
    CommonModule,
    FilterModule,
    FilterModelingModule,
    RatingModule,
    RatingModelingModule,
    RatingExecutionModule,
    NgxSpinnerModule
  ],
  exports: [
    TranslocoRootModule,
    TranslocoModule,
    MaterialModule,
    ClarityModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UserInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
