import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Config } from './APIConfigs/Config';
import {ApiConfig} from "./APIConfigs/ApiConfig";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  apiConfig: ApiConfig = new ApiConfig();
  config: Config = new Config();
  constructor(
    private http: HttpClient
  ) { }

  loadConfig() {
    return this.http.get<Config>('./assets/config/config.json').subscribe(res => {
      console.log(res);
      this.config = res;
    });
  }
}
