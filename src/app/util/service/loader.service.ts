import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private loadCounter: number = 0;

  constructor() { }

  get(): Observable<boolean> {
    return this.loading.asObservable();
  }

  set(loading: boolean) {
    this.loading.next(loading);
  }

  load() {
    this.loadCounter++;
    this.set(true);
  }

  finish() {
    if (this.loadCounter > 0) {
      this.loadCounter--;
    }
    if (this.loadCounter === 0) {
      this.set(false);
    }
  }
}
