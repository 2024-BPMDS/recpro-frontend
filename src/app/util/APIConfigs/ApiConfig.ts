import {RecproConfig} from "./recpro/RecproConfig";
import {BpmsConfig} from "./bpms/BpmsConfig";
import {ModelingConfig} from "./modeling/ModelingConfig";

export class ApiConfig {
  api: Api;
  bpms: BpmsConfig;
  recpro: RecproConfig;

  modeling: ModelingConfig;

  constructor() {
    this.api = new Api();
    this.bpms = new BpmsConfig(this.api.baseUrl);
    this.recpro = new RecproConfig(this.api.baseUrl);
    this.modeling = new ModelingConfig(this.api.baseUrl);
  }
}

class Api {
  baseUrl: string = 'http://localhost:8080/api';
}
