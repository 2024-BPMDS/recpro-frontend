export class BpmsConfig {
  api: BpmsApi;

  constructor(baseUrl: string) {
    this.api = new BpmsApi(baseUrl);
  }
}

class BpmsApi {
  baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }
}
