import {FilterExecutionConfig} from "./execution/FilterExecutionConfig";

export class FilterConfig {

  api: FilterApi;

  execution: FilterExecutionConfig;

  constructor(baseUrl: string) {
    this.api = new FilterApi(baseUrl);
    this.execution = new FilterExecutionConfig(this.api.baseUrl);
  }
}

class FilterApi {
  baseUrl: string;
  filter: Filter = new Filter();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.filter.baseUrl;
  }
}

class Filter {
  baseUrl: string = '/filter';
}
