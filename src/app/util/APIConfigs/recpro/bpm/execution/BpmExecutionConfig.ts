import {BpmExecutionTaskConfig} from "./BpmExecutionTaskConfig";
import {BpmExecutionTasklistConfig} from "./BpmExecutionTasklistConfig";
import {BpmExecutionProcessConfig} from "./BpmExecutionProcessConfig";

export class BpmExecutionConfig {
  api: BpmExecutionApi;
  task: BpmExecutionTaskConfig;
  tasklist: BpmExecutionTasklistConfig;
  process: BpmExecutionProcessConfig;

  constructor(baseUrl: string) {
    this.api = new BpmExecutionApi(baseUrl);
    this.task = new BpmExecutionTaskConfig(this.api.baseUrl);
    this.tasklist = new BpmExecutionTasklistConfig(this.api.baseUrl);
    this.process = new BpmExecutionProcessConfig(this.api.baseUrl);
  }
}

class BpmExecutionApi {
  baseUrl: string;
  execution: Execution = new Execution();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.execution.baseUrl;
  }
}

class Execution {
  baseUrl: string = '/execution';
}
