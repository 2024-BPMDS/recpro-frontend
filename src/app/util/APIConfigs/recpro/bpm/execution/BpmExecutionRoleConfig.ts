import {OntologyConfig} from "../../ontology/OntologyConfig";

export class BpmExecutionRoleConfig {
  api: RoleApi;
  ontologyConfig: OntologyConfig;

  constructor() {

  }
}

class RoleApi {
  baseUrl: string;

  constructor(baseUrl: string) {

  }
}

class Role {
  createAttribute = '/createAttribute';
}
