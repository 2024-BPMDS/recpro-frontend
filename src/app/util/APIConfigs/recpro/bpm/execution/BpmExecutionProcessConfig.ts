export class BpmExecutionProcessConfig {

  processApi: ProcessApi;

  constructor(baseUrl: string) {
    this.processApi = new ProcessApi(baseUrl);
  }

  private getProcessBaseUrl(): string {
    return this.processApi.baseUrl;
  }

  start(processId: string): string {
    return this.getProcessBaseUrl() + this.processApi.process.start + processId;
  }

}

class ProcessApi {
  baseUrl: string;
  process: Process = new Process();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.process.baseUrl;
  }
}

class Process {
  baseUrl: string = '/process';
  start: string = '/start/';
}
