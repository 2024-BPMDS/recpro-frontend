import {AttributeConfig} from "./attribute/AttributeConfig";
import {BpmConfig} from "./bpm/BpmConfig";
import {FilterConfig} from "./filter/FilterConfig";
import {OntologyConfig} from "./ontology/OntologyConfig";
import {RatingConfig} from "./rating/RatingConfig";

export class RecproConfig {

  api: RecproApi;

  attribute: AttributeConfig;
  bpm: BpmConfig;
  filter: FilterConfig;
  ontology: OntologyConfig;
  rating: RatingConfig;

  constructor(baseUrl: string) {
    this.api = new RecproApi(baseUrl);
    this.attribute = new AttributeConfig(this.api.baseUrl);
    this.bpm = new BpmConfig(this.api.baseUrl);
    this.filter = new FilterConfig(this.api.baseUrl);
    this.ontology = new OntologyConfig(this.api.baseUrl);
    this.rating = new RatingConfig(this.api.baseUrl);
  }
}

class RecproApi {
  baseUrl: string;
  recpro: Recpro = new Recpro();
  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.recpro.baseUrl;
  }
}

class Recpro {
  baseUrl: string = '/recpro';
}

