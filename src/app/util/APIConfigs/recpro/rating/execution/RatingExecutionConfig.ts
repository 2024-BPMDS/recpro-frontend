export class RatingExecutionConfig {

  api: RatingExecutionApi;

  constructor(baseUrl: string) {
    this.api = new RatingExecutionApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  init(bpmElementId: string, elementInstanceId: string, processInstanceId: string): string {
    return this.getBaseUrl() + this.api.ratingExecution.init +
      this.api.ratingExecution.bpmElementId + bpmElementId +
      this.api.ratingExecution.elementInstanceId + elementInstanceId +
      this.api.ratingExecution.processInstanceId + processInstanceId;
  }
}

class RatingExecutionApi {
  baseUrl: string;
  ratingExecution: RatingExecution = new RatingExecution();
  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.ratingExecution.baseUrl;
  }
}

class RatingExecution {
  baseUrl: string = '/execution';
  init: string = '/init';
  bpmElementId: string = '?bpmElementId=';
  elementInstanceId: string = '&elementInstanceId=';
  processInstanceId: string = '&processInstanceId=';
}
