import {RatingExecutionConfig} from "./execution/RatingExecutionConfig";
import {RatingPersistenceConfig} from "./persistence/RatingPersistenceConfig";

export class RatingConfig {

  api: RatingApi;
  execution: RatingExecutionConfig;
  persistence: RatingPersistenceConfig;

  constructor(baseUrl: string) {
    this.api = new RatingApi(baseUrl);
    this.persistence = new RatingPersistenceConfig(this.api.baseUrl);
    this.execution = new RatingExecutionConfig(this.api.baseUrl);
  }
}

class RatingApi {
  baseUrl: string;
  rating: Rating = new Rating();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.rating.baseUrl;
  }
}

class Rating {
  baseUrl: string = '/rating';
}
