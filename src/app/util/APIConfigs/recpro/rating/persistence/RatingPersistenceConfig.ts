export class RatingPersistenceConfig {

  api: RatingPersistenceApi;

  constructor(baseUrl: string) {
    this.api = new RatingPersistenceApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  getAll(): string {
    return this.getBaseUrl() + this.api.ratingPersistence.getAll;
  }

  save(): string {
    return this.getBaseUrl() + this.api.ratingPersistence.save;
  }
}

class RatingPersistenceApi {
  baseUrl: string;
  ratingPersistence: RatingPersistence = new RatingPersistence();
  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.ratingPersistence.baseUrl;
  }
}

class RatingPersistence {
  baseUrl: string = '/persistence';
  save: string = '/save';
  getAll: string = '/getAll';
}
