export class OntologyExecutionConfig {
  api: OntologyExecutionApi;

  constructor(baseUrl: string) {
    this.api = new OntologyExecutionApi(baseUrl);
  }
}

class OntologyExecutionApi {
  baseUrl: string;
  execution: OntologyExecution = new OntologyExecution();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.execution.baseUrl;
  }
}

class OntologyExecution {
  baseUrl: string = '/execution';
}
