import {OntologyExecutionConfig} from "./execution/OntologyExecutionConfig";

export class OntologyConfig {

  api: OntologyApi;
  execution: OntologyExecutionConfig;

  constructor(baseUrl: string) {
    this.api = new OntologyApi(baseUrl);
    this.execution = new OntologyExecutionConfig(this.api.baseUrl);
  }

  clearOntology(): string {
    return this.api.baseUrl + this.api.ontology.clear
  }

}

class OntologyApi {
  baseUrl: string;
  ontology: Ontology = new Ontology();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.ontology.baseUrl
  }
}

class Ontology {
  baseUrl: string = '/ontology';
  clear: string = '/clear';
}
