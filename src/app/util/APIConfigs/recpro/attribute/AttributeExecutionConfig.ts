export class AttributeExecutionConfig {

  api: AttributeExecutionApi;

  private getBasUrl(): string {
    return this.api.baseUrl;
  }

  create(): string {
    return this.getBasUrl() + this.api.attributeExecution.create;
  }

  createAttribute(): string {
    return this.getBasUrl() + this.api.attributeExecution.createAttribute;
  }

  constructor(baseUrl: string) {
    this.api = new AttributeExecutionApi(baseUrl);
  }
}

class AttributeExecutionApi {
  baseUrl: string;
  attributeExecution: AttributeExecution = new AttributeExecution();
  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.attributeExecution.baseUrl;
  }
}

class AttributeExecution {
  baseUrl: string = '/execution';
  create: string = '/create';
  createAttribute: string = '/createAttribute';
}
