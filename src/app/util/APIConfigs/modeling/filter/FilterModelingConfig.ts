import {FilterType} from "../../../../model/RecPro/filter/FilterType";

export class FilterModelingConfig {
  api: FilterModelingApi;

  constructor(baseUrl: string) {
    this.api = new FilterModelingApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  create(): string {
    return this.getBaseUrl() + this.api.filterModeling.create;
  }

  getAll(): string {
    return this.getBaseUrl() + this.api.filterModeling.getAll;
  }

  getById(id: string): string {
    return this.getBaseUrl() + this.api.filterModeling.getById + id;
  }

  delete(id: string): string {
    return this.getBaseUrl() + this.api.filterModeling.delete + id;
  }

  getByType(type: FilterType): string {
    return this.getBaseUrl() + this.api.filterModeling.getByType + type.valueOf();
  }
}

class FilterModelingApi {
  baseUrl: string;
  filterModeling: FilterModeling = new FilterModeling();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.filterModeling.baseUrl;
  }
}

class FilterModeling {
  baseUrl: string = '/filter'
  create: string = '/create';
  getAll: string = '/getAll';
  getById: string = '/getById/';
  delete: string = '/delete/';
  getByType: string = '/getByType/';

}
