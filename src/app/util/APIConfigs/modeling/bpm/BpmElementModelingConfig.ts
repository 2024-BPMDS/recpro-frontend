export class BpmElementModelingConfig {

  api: BpmElementModelingApi;

  constructor(baseUrl: string) {
    this.api = new BpmElementModelingApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  getAll(): string {
    return this.getBaseUrl() + this.api.bpmElement.getAll;
  }

}

class BpmElementModelingApi {
  baseUrl: string;
  bpmElement: BpmElement = new BpmElement();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.bpmElement.baseUrl;
  }
}

export class BpmElement {
  baseUrl: string = '/element';
  getAll: string = '/getAll'
}
