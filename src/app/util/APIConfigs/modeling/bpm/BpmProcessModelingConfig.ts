export class BpmProcessModelingConfig {
  api: ProcessModelingApi;

  constructor(baseUrl: string) {
    this.api = new ProcessModelingApi(baseUrl);
  }

  private getBaseUrl(): string {
    return this.api.baseUrl;
  }

  getAll(): string {
    return this.getBaseUrl() + this.api.process.getAll;
  }

  getLatest(): string {
    return this.getBaseUrl() + this.api.process.getLatest;
  }

  createAll(): string {
    return this.getBaseUrl() + this.api.process.createAll;
  }

  getById(id: string): string {
    return this.getBaseUrl() + this.api.process.getById + id;
  }

  getByActivityId(id: string): string {
    return this.getBaseUrl() + this.api.process.getByActivityId + id;
  }

  getByAttributeId(id: string): string {
    return this.getBaseUrl() + this.api.process.getByAttributeId + id;
  }
}

class ProcessModelingApi {
  baseUrl: string;
  process: Process = new Process();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.process.baseUrl;
  }
}

class Process {
  baseUrl: string = '/process';
  getAll: string = '/getAll';
  getLatest: string = '/getLatest';
  createAll: string = '/createAll';
  getById: string = '/getById/'
  getByActivityId: string = '/getByActivityId/';
  getByAttributeId: string = '/getByAttributeId/';
}
