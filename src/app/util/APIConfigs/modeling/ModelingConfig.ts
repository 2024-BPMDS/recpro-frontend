import {UserModelingConfig} from "./user/UserModelingConfig";
import {OntologyModelingConfig} from "./ontology/OntologyModelingConfig";
import {RatingModelingConfig} from "./rating/RatingModelingConfig";
import {BpmModelingConfig} from "./bpm/BpmModelingConfig";
import {AttributeModelingConfig} from "./attribute/AttributeModelingConfig";
import {FilterModelingConfig} from "./filter/FilterModelingConfig";

export class ModelingConfig {

  api: ModelingApi;
  user: UserModelingConfig;
  ontology: OntologyModelingConfig;
  rating: RatingModelingConfig;
  bpm: BpmModelingConfig;
  attribute: AttributeModelingConfig;
  filter: FilterModelingConfig;

  constructor(baseUrl: string) {
    this.api = new ModelingApi(baseUrl);
    this.user = new UserModelingConfig(this.api.baseUrl);
    this.ontology = new OntologyModelingConfig(this.api.baseUrl);
    this.rating = new RatingModelingConfig(this.api.baseUrl);
    this.bpm = new BpmModelingConfig(this.api.baseUrl);
    this.attribute = new AttributeModelingConfig(this.api.baseUrl);
    this.filter = new FilterModelingConfig(this.api.baseUrl);
  }
}

class ModelingApi {
  baseUrl: string;
  modeling: Modeling = new Modeling();

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl + this.modeling.baseUrl;
  }
}

class Modeling {
  baseUrl: string = '/modeling';
}
