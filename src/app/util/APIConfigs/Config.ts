import {KeycloakConfiguration} from "../../model/keycloakConfiguration";
import {BpmConfig} from "./recpro/bpm/BpmConfig";
import {AttributeConfig} from "./recpro/attribute/AttributeConfig";

export class Config {

  production: boolean = false;
  serverUrl: string;
  api: Api;
  keycloak: KeycloakConfiguration;
  bpmConfig: BpmConfig;
  attributeConfig: AttributeConfig;

  constructor() {
    this.api = new Api();
    this.production = false;
    this.serverUrl = '';
    this.keycloak = new KeycloakConfiguration('', '', '');
    this.bpmConfig = new BpmConfig(this.api.baseUrl);
    this.attributeConfig = new AttributeConfig(this.api.baseUrl);
  }

  public getBaseUrl(): string {
    return this.api.baseUrl;
  }

  private getOntologyBaseUrl(): string {
    return this.getBaseUrl() + this.api.ontology.baseUrl;
  }

  private getBpmBaseUrl(): string {
    return this.getOntologyBaseUrl() + this.api.bpmBaseUrl;
  }

  public getRoleBaseUrl(): string {
    return this.getOntologyBaseUrl();
  }

  private getOntology(): string {
    return this.getBaseUrl() + this.api.ontology.baseUrl;
  }

  private getOntologyProcess(): string {
    return this.getOntology() + this.api.ontology.process.baseUrl;
  }

  private getOntologyActivity(): string {
    return this.getOntology() + this.api.ontology.activity.baseUrl;
  }

  getOntologyProcessesByIds(): string {
    return this.getOntologyProcess() + this.api.ontology.process.getProcessesByIds;
  }

  getOntologyActivityByIds(): string {
    return this.getOntologyActivity() + this.api.ontology.activity.getActivitiesByIds;
  }

  getOntologyActivityById(): string {
    return this.getOntologyActivity() + this.api.ontology.activity.getActivityById;
  }

  getOntologyActivityByIdAndProcessId(): string {
    return this.getOntologyActivity() + this.api.ontology.activity.getActivityByIdAndProcessId;
  }

  private getOntologyAttributes() {
    return this.getOntology() + this.api.ontology.attribute.baseUrl;
  }

  getOntologyAttributesByIds(): string {
    return this.getOntologyAttributes() + this.api.ontology.attribute.getAttributesByUris;
  }

  getOntologyAttributesByActivityIds(): string {
    return this.getOntologyAttributes() + this.api.ontology.attribute.getAttributesByActivityIds;
  }

  getAttributeInstanceDtosByTaskId(): string {
    return this.getAttributeInstance() + this.api.attributeInstance.getDtoByTaskId;
  }

  private getAttributeInstance(): string {
    return this.getBaseUrl() + this.api.attributeInstance.baseUrl;
  }

  private createAttributeInstance(): string {
    return this.getAttributeInstance() + this.api.attributeInstance.create;
  }

  createBinaryAttributeInstance(): string {
    return this.createAttributeInstance() + this.api.attributeInstance.binary;
  }

  createNumericalAttributeInstance(): string {
    return this.createAttributeInstance() + this.api.attributeInstance.numerical;
  }

  createTextualAttributeInstance(): string {
    return this.createAttributeInstance() + this.api.attributeInstance.textual;
  }

  getAttributeInstanceByTaskId(): string {
    return this.getAttributeInstance() + this.api.attributeInstance.getByTaskId;
  }

  getBinaryAttributeInstanceByTaskId(): string {
    return this.getAttributeInstanceByTaskId() + this.api.attributeInstance.binary;
  }

  getNumericalAttributeInstanceByTaskId(): string {
    return this.getAttributeInstanceByTaskId() + this.api.attributeInstance.numerical;
  }

  getTextualAttributeInstanceByTaskId(): string {
    return this.getAttributeInstanceByTaskId() + this.api.attributeInstance.textual;
  }

}

class Api {
  baseUrl: string = 'http://localhost:8080/api';
  bpmBaseUrl: string = '/bpm';

  bpmModelingBaseUrl: string = '/modeling';
  bpmExecutionBaseUrl: string = '/execution';
  roleBaseUrl: string = '/role';
  processBaseUrl: string = '/process';
  taskBaseUrl: string = '/task';

  bpm: Bpm = new Bpm();
  ontology: Ontology = new Ontology();
  attribute: Attribute = new Attribute();
  attributeInstance: AttributeInstance = new AttributeInstance();
}

class Bpm {
  baseUrl: string = '/bpm';
  tasklist: Tasklist = new Tasklist();
}

class Tasklist {
  baseUrl: string = '/tasklist';
  getByAssignee: string = '/getByAssignee';
  getDtoByAssignee: string = "/getByAssignee/recproTasklistDto";
}

class Ontology {
  baseUrl: string = '/ontology';
  process: Process = new Process();
  activity: Activity = new Activity();
  attribute: Attribute = new Attribute();
}

class Process {
  baseUrl: string = '/process';
  getProcessesByIds: string = '/getProcessesByIds';
}

class Activity {
  baseUrl: string = '/activity';
  getActivitiesByIds: string = '/getActivitiesByIds';
  getActivityById: string = '/getActivityById';
  getActivityByIdAndProcessId: string = '/getActivityByIdAndProcessId';
}

class Attribute {
  baseUrl: string = '/attribute'
  getAttributesByUris: string = '/getAttributesByUris';
  getAttributesByActivityIds: string = '/getAttributesByActivityIds';
}

class AttributeInstance {
  baseUrl: string = '/attribute';
  getByTaskIds: string = '/getByTaskIds';
  getByTaskId: string = '/getByTaskId';
  getByIds: string = '/getByIds';
  getById: string = '/getById';
  create: string = '/create';
  numerical: string = '/numerical';
  binary: string = '/binary';
  textual: string = '/textual';
  getDtoByTaskId: string = '/getAttributesByTaskId';
}
