import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {UserComponent} from "./components/user/user.component";
import {BpmComponent} from "./components/bpm/bpm.component";
import {BpmModelingComponent} from "./components/bpm/bpm-modeling/bpm-modeling.component";
import {BpmExecutionComponent} from "./components/bpm/bpm-execution/bpm-execution.component";
import {ErrorComponent} from "./layout/error/error.component";
import {ActivityComponent} from "./components/bpm/bpm-modeling/recpro-element/activity/activity.component";
import {ProcessComponent} from "./components/bpm/bpm-modeling/recpro-element/process/process.component";
import {
  RatingModelingBaseComponent
} from "./components/rating/rating-modeling/rating-modeling-base/rating-modeling-base.component";
import {OntologyBaseComponent} from "./components/ontology/ontology-base/ontology-base.component";
import {FilterModelingComponent} from "./components/filter/filter-modeling/filter-modeling.component";
import {AttributeComponent} from "./components/attribute/attribute.component";
import {AttributeModelingComponent} from "./components/attribute/attribute-modeling/attribute-modeling.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'user', component: UserComponent},
  { path: 'bpm', component: BpmComponent },
  { path: 'bpm/modeling', component: BpmModelingComponent },
  { path: 'bpm/modeling/activity', component: ActivityComponent },
  { path: 'bpm/modeling/process', component: ProcessComponent },
  { path: 'bpm/execution', component: BpmExecutionComponent },
  { path: 'filter/modeling/all', component: FilterModelingComponent},
  { path: 'attribute', component: AttributeComponent },
  { path: 'attribute/modeling', component: AttributeModelingComponent },
  { path: 'modeling/attribute', component: AttributeModelingComponent },
  { path: 'rating/modeling', component: RatingModelingBaseComponent },
  { path: 'modeling/ontology', component: OntologyBaseComponent },
  { path: 'modeling/users', component: UserComponent },
  { path: 'error', component: ErrorComponent },
  { path: '**', redirectTo: 'error' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
