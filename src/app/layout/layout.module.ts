import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarHeaderComponent } from './navbar-header/navbar-header.component';
import {ClarityModule} from "@clr/angular";
import { NavbarVerticalComponent } from './navbar-vertical/navbar-vertical.component';
import { ContentContainerComponent } from './content-container/content-container.component';
import { AlertComponent } from './alert/alert.component';
import {RouterModule} from "@angular/router";
import { ErrorComponent } from './error/error.component';



@NgModule({
  declarations: [
    NavbarHeaderComponent,
    NavbarVerticalComponent,
    ContentContainerComponent,
    AlertComponent,
    ErrorComponent
  ],
  exports: [
    NavbarHeaderComponent,
    ContentContainerComponent,
    AlertComponent
  ],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule.forRoot([], {useHash: true}),
  ]
})
export class LayoutModule { }
