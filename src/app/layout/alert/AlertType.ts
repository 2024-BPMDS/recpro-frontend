
export enum AlertType {
  info,
  danger,
  success,
  warning
}
