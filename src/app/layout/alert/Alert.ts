import {AlertType} from "./AlertType";

export class Alert {
  private message: string;
  private type: string;

  constructor(message: string, type: AlertType) {
    this.message = message;
    this.type = AlertType[type];
  }

  public getMessage(): string {
    return this.message;
  }

  public setMessage(message: string) {
    this.message = message;
  }

  public getType(): string {
    return this.type;
  }

  public setType(type: string) {
    this.type = type;
  }
}
