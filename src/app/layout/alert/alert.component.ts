import {Component} from '@angular/core';
import {Alert} from "./Alert";
import {AlertType} from "./AlertType";

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent {

  alerts: Alert[] = [];

  constructor() {
  }

  addAlert(message: string, type: AlertType) {
    this.alerts.push(new Alert(message, type));
  }

}
