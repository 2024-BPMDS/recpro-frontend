import {RecproGroup} from "./RecproGroup";

export class RecproUser {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  // attributes: RecproAttributeInstance[];
  groups: RecproGroup[];

  constructor() {
    this.id = '-1';
    this.firstName = '';
    this.lastName = '';
    // this.attributes = [];
    this.email = '';
    this.password = '';
    this.groups = [];
  }
}
