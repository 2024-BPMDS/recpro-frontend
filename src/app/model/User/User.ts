import {RecproRole} from "../BPM/modeling/RecproRole";

export class User {
  id: string;
  lastName: string;
  firstName: string;
  description: string;
  roles: RecproRole[] = [];

  constructor() {
    this.id = '';
    this.lastName = '';
    this.firstName = '';
    this.description = '';
    this.roles = [];
  }
}
