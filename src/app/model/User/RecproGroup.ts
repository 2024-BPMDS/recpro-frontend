export class RecproGroup {
  id: string;
  name: string;
  description: string;
  type: string;

  constructor(id: string, name: string, description: string, type: string) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.type = type;
  }
}
