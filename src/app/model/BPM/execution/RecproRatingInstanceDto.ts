import {RecproElementInstanceDto} from "./RecproElementInstanceDto";

export class RecproRatingInstanceDto extends RecproElementInstanceDto {
  constructor(id: number, assigneeId: string, likeliness: number) {
    super();
  }
}
