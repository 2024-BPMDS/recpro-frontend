import {RecproElementInstanceDto} from "./RecproElementInstanceDto";

export class RecproProcessInstanceDto extends RecproElementInstanceDto {
  bpmsProcessInstanceId: string;
  processName: string;

  constructor() {
    super();
    this.bpmsProcessInstanceId = '';
    this.processName = '';
  }
}
