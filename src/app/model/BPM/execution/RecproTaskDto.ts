import {RecproElementInstanceDto} from "./RecproElementInstanceDto";
import {RecproTaskState} from "./RecproTaskState";
import {RecproActivity} from "../modeling/RecproActivity";
import {RecproProcessInstanceDto} from "./RecproProcessInstanceDto";

export class RecproTaskDto extends RecproElementInstanceDto {
  taskId: string;
  position: number;
  state: RecproTaskState;
  activity: RecproActivity;
  processInstance: RecproProcessInstanceDto;
  priority: number;
  createDate: Date;
  editDate: Date;
  dueDate: Date;
  executionId: string;
  visible: boolean;

  constructor() {
    super();
    this.taskId = '';
    this.position = -1;
    this.state = RecproTaskState.OPEN;
    this.activity = new RecproActivity();
    this.processInstance = new RecproProcessInstanceDto();
    this.priority = -1;
    this.createDate = new Date();
    this.editDate = new Date();
    this.dueDate = new Date();
    this.executionId = '';
    this.visible = true;
  }
}
