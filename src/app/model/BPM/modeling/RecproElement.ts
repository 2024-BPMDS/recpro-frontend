import {RecproAttribute} from "../../RecPro/attributes/modeling/RecproAttribute";
import {RecproElementType} from "./RecproElementType";

export class RecproElement {
  id: string;
  name: string;
  description: string;
  attributes: RecproAttribute[] = [];
  elementType: RecproElementType;

  constructor() {
    this.id = '';
    this.name = '';
    this.description = '';
    this.attributes = [];
    this.elementType = RecproElementType.PROCESS;
  }
}
