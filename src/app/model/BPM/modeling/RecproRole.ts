import {RecproElement} from "./RecproElement";
export class RecproRole extends RecproElement{

  childElements: RecproRole[];

  constructor() {
    super();
    this.childElements = [];
  }

}
