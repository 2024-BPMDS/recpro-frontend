import {RecproElement} from "./RecproElement";
import {RecproProcessModel} from "./RecproProcessModel";

export class RecproProcess extends RecproElement {
  version: string;
  key: string;
  latest: boolean;
  processModel: RecproProcessModel;

  constructor() {
    super();
    this.version = '';
    this.key = '';
    this.latest = false;
    this.processModel = new RecproProcessModel();
  }
}
