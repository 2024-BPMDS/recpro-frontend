import {Rating} from "./Rating";

export class BinaryRating extends Rating {

  trueLabel: string;
  falseLabel: string;
  defaultValue: boolean;

  constructor() {
    super();
    this.trueLabel = '';
    this.falseLabel = '';
    this.defaultValue = false;
  }
}
