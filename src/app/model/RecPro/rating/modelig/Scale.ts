import {ScaleDetail} from "./ScaleDetail";

export class Scale {
  id: string;
  details: ScaleDetail[];

  constructor() {
    this.id = '';
    this.details = [];
  }
}
