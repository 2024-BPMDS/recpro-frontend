import {Rating} from "./Rating";
import {Scale} from "./Scale";

export class OrdinalRating  extends Rating {
  scale: Scale;
  defaultValue: number;

  constructor() {
    super();
    this.scale = new Scale();
    this.defaultValue = -1;
  }
}
