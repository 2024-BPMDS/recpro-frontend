import {Rating} from "./Rating";

export class UnaryRating extends Rating {
  label: string;
  defaultValue: boolean;

  constructor() {
    super();
    this.label = '';
    this.defaultValue = false;
  }
}
