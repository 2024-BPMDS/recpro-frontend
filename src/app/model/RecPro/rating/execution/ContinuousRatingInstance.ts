import {RatingInstance} from "./RatingInstance";

export class ContinuousRatingInstance extends RatingInstance {
  value: number;

  constructor() {
    super();
    this.value = -1;
  }
}
