import {RatingInstance} from "./RatingInstance";

export class IntervalBasedRatingInstance extends RatingInstance {
  value: number;

  constructor() {
    super();
    this.value = -1;
  }
}
