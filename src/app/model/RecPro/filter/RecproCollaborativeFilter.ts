import {RecproFilter} from "./RecproFilter";
import {FilterType} from "./FilterType";
import {RecproElement} from "../../BPM/modeling/RecproElement";
import {RecproAttribute} from "../attributes/modeling/RecproAttribute";
import {Rating} from "../rating/modelig/Rating";
import {User} from "../../User/User";

export class RecproCollaborativeFilter extends RecproFilter {

  bpmElements: RecproElement[] = [];
  attributes: RecproAttribute[] = [];
  ratings: Rating[] = [];
  users: User[] = [];
  allInputElements: boolean = false;
  allInputAttributes: boolean = false;
  allInputRatings: boolean = false;
  allInputUsers: boolean = false;

  constructor() {
    super();
    this.filterType = FilterType.COLLABORATIVE;
    this.bpmElements = [];
    this.attributes = [];
    this.ratings = [];
    this.users = [];
    this.allInputAttributes = false;
    this.allInputElements = false;
    this.allInputRatings = false;
    this.allInputUsers = false;
  }

}
