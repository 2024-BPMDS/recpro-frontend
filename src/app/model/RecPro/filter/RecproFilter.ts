import {FilterType} from "./FilterType";

export class RecproFilter {
  id: string;
  name: string;
  description: string;
  filterUrl: string;
  filterType: FilterType;

  constructor() {
    this.id = '';
    this.name = '';
    this.description = '';
    this.filterUrl = '';
    this.filterType = FilterType.KNOWLEDGE_BASED;
  }
}
