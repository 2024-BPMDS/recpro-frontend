import {RecproFilter} from "./RecproFilter";
import {TasklistOrder} from "./TasklistOrder";
import {FilterType} from "./FilterType";

export class RecproBaseFilter extends RecproFilter {
  ascending: boolean = true;
  tasklistOrder: TasklistOrder;

  constructor() {
    super();
    this.filterType = FilterType.BASE;
    this.tasklistOrder = TasklistOrder.CREATE_DATE;
  }
}
