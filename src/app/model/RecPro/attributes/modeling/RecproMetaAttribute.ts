import {RecproAttribute} from "./RecproAttribute";
import {RecproAttributeType} from "./RecproAttributeType";

export class RecproMetaAttribute extends RecproAttribute {
  override attributeType: RecproAttributeType = RecproAttributeType.META;
}
