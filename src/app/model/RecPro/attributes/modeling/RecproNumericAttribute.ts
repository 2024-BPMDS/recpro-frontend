import {RecproAttribute} from "./RecproAttribute";
import {RecproAttributeType} from "./RecproAttributeType";

export class RecproNumericAttribute extends RecproAttribute {
  defaultValue: number = -1;
  override attributeType: RecproAttributeType = RecproAttributeType.NUMERIC;

  constructor() {
    super();
  }
}
