import {RecproAttribute} from "./RecproAttribute";
import {RecproAttributeType} from "./RecproAttributeType";

export class RecproBinaryAttribute extends RecproAttribute {
  defaultValue: boolean = false;
  override attributeType: RecproAttributeType = RecproAttributeType.BINARY;

  constructor() {
    super();
  }
}
