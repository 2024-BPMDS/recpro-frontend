import {RecproAttribute} from "./modeling/RecproAttribute";
import {RecproAttributeInstance} from "./execution/RecproAttributeInstance";

export class RecproAttributeInstanceDto {
  public attribute: RecproAttribute;
  public instance: RecproAttributeInstance;

  constructor() {
    this.attribute = new RecproAttribute();
    this.instance = new RecproAttributeInstance();
  }
}
