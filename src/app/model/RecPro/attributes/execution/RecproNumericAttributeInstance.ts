import {RecproAttributeInstance} from "./RecproAttributeInstance";

export class RecproNumericAttributeInstance extends RecproAttributeInstance {
  value: number;

  constructor() {
    super();
    this.value = -1;
  }
}
