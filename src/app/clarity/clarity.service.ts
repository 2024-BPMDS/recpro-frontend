import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ClarityIcons} from "@cds/core/icon";

@Injectable({
  providedIn: 'root'
})
export class ClarityService {

  constructor(private http: HttpClient) { }

  loadCustomIcon(name: string, path: string) {
    this.http.get(path, {responseType: 'text'}).subscribe((svg) => {
      ClarityIcons.addIcons([name, svg]);
    });

  }
}
