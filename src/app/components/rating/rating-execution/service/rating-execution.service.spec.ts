import { TestBed } from '@angular/core/testing';

import { RatingExecutionService } from './rating-execution.service';

describe('RatingExecutionService', () => {
  let service: RatingExecutionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RatingExecutionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
