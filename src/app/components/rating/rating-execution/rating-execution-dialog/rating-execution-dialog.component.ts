import {Component, Inject, OnInit} from '@angular/core';
import {RatingDto} from "../../../../model/RecPro/rating/dto/RatingDto";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {RatingExecutionService} from "../service/rating-execution.service";
import {RecproTaskDto} from "../../../../model/BPM/execution/RecproTaskDto";
import {BinaryRatingInstance} from "../../../../model/RecPro/rating/execution/BinaryRatingInstance";
import {BinaryRating} from "../../../../model/RecPro/rating/modelig/BinaryRating";
import {ContinuousRating} from "../../../../model/RecPro/rating/modelig/ContinuousRating";
import {ContinuousRatingInstance} from "../../../../model/RecPro/rating/execution/ContinuousRatingInstance";
import {IntervalBasedRating} from "../../../../model/RecPro/rating/modelig/IntervalBasedRating";
import {IntervalBasedRatingInstance} from "../../../../model/RecPro/rating/execution/IntervalBasedRatingInstance";
import {OrdinalRating} from "../../../../model/RecPro/rating/modelig/OrdinalRating";
import {OrdinalRatingInstance} from "../../../../model/RecPro/rating/execution/OrdinalRatingInstance";
import {UnaryRating} from "../../../../model/RecPro/rating/modelig/UnaryRating";
import {UnaryRatingInstance} from "../../../../model/RecPro/rating/execution/UnaryRatingInstance";
import {LoaderService} from "../../../../util/service/loader.service";

@Component({
  selector: 'app-rating-execution-dialog',
  templateUrl: './rating-execution-dialog.component.html',
  styleUrls: ['./rating-execution-dialog.component.css']
})
export class RatingExecutionDialogComponent implements OnInit {
  rating: RatingDto = new RatingDto();

  constructor(
    private dialogRef: MatDialogRef<RatingExecutionDialogComponent>,
    private ratingExecutionService: RatingExecutionService,
    @Inject(MAT_DIALOG_DATA) public bpmElement: RecproTaskDto,
    private loaderService: LoaderService
  ) {
  }

  ngOnInit() {
    this.loaderService.load();
    this.ratingExecutionService.init(this.bpmElement.recproElementId, this.bpmElement.recproElementInstanceId, this.bpmElement.processInstance.bpmsProcessInstanceId).subscribe(res => {
      this.rating = res;
      this.loaderService.finish();
    });
  }

  log() {
    console.log(this.rating);
    console.log(this.bpmElement);
  }

  cancel() {
    this.dialogRef.close(false);
  }

  save() {
    this.loaderService.load();
    this.ratingExecutionService.save(this.rating.instance).subscribe(() => {
      this.dialogRef.close(true);
      this.loaderService.finish();
    });
  }

  getBinaryRating(): BinaryRating {
    return this.rating.rating as BinaryRating;
  }

  getBinaryRatingInstance(): BinaryRatingInstance {
    return this.rating.instance as BinaryRatingInstance;
  }

  getContinuousRating(): ContinuousRating {
    return this.rating.rating as ContinuousRating;
  }

  getContinuousRatingInstance(): ContinuousRatingInstance {
    return this.rating.instance as ContinuousRatingInstance;
  }

  getIntervalBasedRating(): IntervalBasedRating {
    return this.rating.rating as IntervalBasedRating;
  }

  getIntervalBasedRatingInstance(): IntervalBasedRatingInstance {
    return this.rating.instance as IntervalBasedRatingInstance;
  }

  getOrdinalRating(): OrdinalRating {
    return this.rating.rating as OrdinalRating;
  }

  getOrdinalRatingInstance(): OrdinalRatingInstance {
    return this.rating.instance as OrdinalRatingInstance;
  }

  getUnaryRating(): UnaryRating {
    return this.rating.rating as UnaryRating;
  }

  getUnaryRatingInstance(): UnaryRatingInstance {
    return this.rating.instance as UnaryRatingInstance;
  }

}
