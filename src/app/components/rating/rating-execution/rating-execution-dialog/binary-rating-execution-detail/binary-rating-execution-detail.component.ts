import {Component, Input} from '@angular/core';
import {BinaryRatingInstance} from "../../../../../model/RecPro/rating/execution/BinaryRatingInstance";
import {BinaryRating} from "../../../../../model/RecPro/rating/modelig/BinaryRating";

@Component({
  selector: 'app-binary-rating-execution-detail',
  templateUrl: './binary-rating-execution-detail.component.html',
  styleUrls: ['./binary-rating-execution-detail.component.css']
})
export class BinaryRatingExecutionDetailComponent {

  @Input() binaryRatingInstance: BinaryRatingInstance = new BinaryRatingInstance();
  @Input() binaryRating: BinaryRating = new BinaryRating();

}
