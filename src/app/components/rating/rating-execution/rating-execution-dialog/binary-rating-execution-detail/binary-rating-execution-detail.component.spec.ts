import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryRatingExecutionDetailComponent } from './binary-rating-execution-detail.component';

describe('BinaryRatingExecutionDetailComponent', () => {
  let component: BinaryRatingExecutionDetailComponent;
  let fixture: ComponentFixture<BinaryRatingExecutionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BinaryRatingExecutionDetailComponent]
    });
    fixture = TestBed.createComponent(BinaryRatingExecutionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
