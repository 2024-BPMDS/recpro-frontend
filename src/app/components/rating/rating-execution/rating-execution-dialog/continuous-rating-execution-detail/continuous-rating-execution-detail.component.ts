import {Component, Input} from '@angular/core';
import {ContinuousRating} from "../../../../../model/RecPro/rating/modelig/ContinuousRating";
import {ContinuousRatingInstance} from "../../../../../model/RecPro/rating/execution/ContinuousRatingInstance";

@Component({
  selector: 'app-continuous-rating-execution-detail',
  templateUrl: './continuous-rating-execution-detail.component.html',
  styleUrls: ['./continuous-rating-execution-detail.component.css']
})
export class ContinuousRatingExecutionDetailComponent {

  @Input() continuousRating: ContinuousRating = new ContinuousRating();
  @Input() continuousRatingInstance: ContinuousRatingInstance = new ContinuousRatingInstance();
}
