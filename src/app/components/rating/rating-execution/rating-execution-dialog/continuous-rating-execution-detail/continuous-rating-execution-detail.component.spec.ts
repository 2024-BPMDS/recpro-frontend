import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinuousRatingExecutionDetailComponent } from './continuous-rating-execution-detail.component';

describe('ContinuousRatingExecutionDetailComponent', () => {
  let component: ContinuousRatingExecutionDetailComponent;
  let fixture: ComponentFixture<ContinuousRatingExecutionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ContinuousRatingExecutionDetailComponent]
    });
    fixture = TestBed.createComponent(ContinuousRatingExecutionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
