import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnaryRatingExecutionDetailComponent } from './unary-rating-execution-detail.component';

describe('UnaryRatingExecutionDetailComponent', () => {
  let component: UnaryRatingExecutionDetailComponent;
  let fixture: ComponentFixture<UnaryRatingExecutionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UnaryRatingExecutionDetailComponent]
    });
    fixture = TestBed.createComponent(UnaryRatingExecutionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
