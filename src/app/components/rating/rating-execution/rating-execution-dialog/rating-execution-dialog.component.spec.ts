import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingExecutionDialogComponent } from './rating-execution-dialog.component';

describe('RatingExecutionDialogComponent', () => {
  let component: RatingExecutionDialogComponent;
  let fixture: ComponentFixture<RatingExecutionDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingExecutionDialogComponent]
    });
    fixture = TestBed.createComponent(RatingExecutionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
