import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntervalBasedRatingExecutionDetailComponent } from './interval-based-rating-execution-detail.component';

describe('IntervalBasedRatingExecutionDetailComponent', () => {
  let component: IntervalBasedRatingExecutionDetailComponent;
  let fixture: ComponentFixture<IntervalBasedRatingExecutionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IntervalBasedRatingExecutionDetailComponent]
    });
    fixture = TestBed.createComponent(IntervalBasedRatingExecutionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
