import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatingExecutionDialogComponent } from './rating-execution-dialog/rating-execution-dialog.component';
import {MaterialModule} from "../../../material/material.module";
import {FormsModule} from "@angular/forms";
import {ClarityModule} from "@clr/angular";
import { BinaryRatingExecutionDetailComponent } from './rating-execution-dialog/binary-rating-execution-detail/binary-rating-execution-detail.component';
import { ContinuousRatingExecutionDetailComponent } from './rating-execution-dialog/continuous-rating-execution-detail/continuous-rating-execution-detail.component';
import { OrdinalRatingExecutionDetailComponent } from './rating-execution-dialog/ordinal-rating-execution-detail/ordinal-rating-execution-detail.component';
import { IntervalBasedRatingExecutionDetailComponent } from './rating-execution-dialog/interval-based-rating-execution-detail/interval-based-rating-execution-detail.component';
import { UnaryRatingExecutionDetailComponent } from './rating-execution-dialog/unary-rating-execution-detail/unary-rating-execution-detail.component';
import {PipeModule} from "../../../util/pipe/pipe.module";

@NgModule({
  declarations: [
    RatingExecutionDialogComponent,
    BinaryRatingExecutionDetailComponent,
    ContinuousRatingExecutionDetailComponent,
    OrdinalRatingExecutionDetailComponent,
    IntervalBasedRatingExecutionDetailComponent,
    UnaryRatingExecutionDetailComponent,
  ],
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ClarityModule,
      PipeModule
    ]
})
export class RatingExecutionModule { }
