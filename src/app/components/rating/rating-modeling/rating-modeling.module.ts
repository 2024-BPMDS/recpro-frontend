import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatingModelingBaseComponent } from './rating-modeling-base/rating-modeling-base.component';
import { RatingCreatorDialogComponent } from './rating-creator-dialog/rating-creator-dialog.component';
import {MaterialModule} from "../../../material/material.module";
import {ClarityModule} from "@clr/angular";
import {FormsModule} from "@angular/forms";
import { BinaryRatingDetailComponent } from './rating-creator-dialog/binary-rating-detail/binary-rating-detail.component';
import { ContinuousRatingDetailComponent } from './rating-creator-dialog/continuous-rating-detail/continuous-rating-detail.component';
import { ScaleModelingComponent } from './rating-creator-dialog/scale-modeling/scale-modeling.component';
import { ScaleDetailModelingComponent } from './rating-creator-dialog/scale-modeling/scale-detail-modeling/scale-detail-modeling.component';
import { IntervalBasedRatingDetailComponent } from './rating-creator-dialog/interval-based-rating-detail/interval-based-rating-detail.component';
import { OrdinalRatingDetailComponent } from './rating-creator-dialog/ordinal-rating-detail/ordinal-rating-detail.component';
import { UnaryRatingDetailComponent } from './rating-creator-dialog/unary-rating-detail/unary-rating-detail.component';



@NgModule({
  declarations: [
    RatingModelingBaseComponent,
    RatingCreatorDialogComponent,
    BinaryRatingDetailComponent,
    ContinuousRatingDetailComponent,
    ScaleModelingComponent,
    ScaleDetailModelingComponent,
    IntervalBasedRatingDetailComponent,
    OrdinalRatingDetailComponent,
    UnaryRatingDetailComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ClarityModule,
    FormsModule
  ]
})
export class RatingModelingModule { }
