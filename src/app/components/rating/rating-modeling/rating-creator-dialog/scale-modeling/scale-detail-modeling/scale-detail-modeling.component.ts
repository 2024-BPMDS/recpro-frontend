import {Component, Input} from '@angular/core';
import {ScaleDetail} from "../../../../../../model/RecPro/rating/modelig/ScaleDetail";

@Component({
  selector: 'app-scale-detail-modeling',
  templateUrl: './scale-detail-modeling.component.html',
  styleUrls: ['./scale-detail-modeling.component.css']
})
export class ScaleDetailModelingComponent {
  @Input() scaleDetails: ScaleDetail[] = [];

  addScaleDetail() {
    this.scaleDetails.push(new ScaleDetail());
  }

  removeDetail(scaleDetail: ScaleDetail) {
    const index = this.scaleDetails.findIndex(element => element.id === scaleDetail.id);
    if (index !== -1) {
      this.scaleDetails.splice(index, 1);
    }
  }
}
