import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScaleDetailModelingComponent } from './scale-detail-modeling.component';

describe('ScaleDetailModelingComponent', () => {
  let component: ScaleDetailModelingComponent;
  let fixture: ComponentFixture<ScaleDetailModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ScaleDetailModelingComponent]
    });
    fixture = TestBed.createComponent(ScaleDetailModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
