import {Component, Input} from '@angular/core';
import {Scale} from "../../../../../model/RecPro/rating/modelig/Scale";

@Component({
  selector: 'app-scale-modeling',
  templateUrl: './scale-modeling.component.html',
  styleUrls: ['./scale-modeling.component.css']
})
export class ScaleModelingComponent {
  @Input() scale: Scale = new Scale();
}
