import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScaleModelingComponent } from './scale-modeling.component';

describe('ScaleModelingComponent', () => {
  let component: ScaleModelingComponent;
  let fixture: ComponentFixture<ScaleModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ScaleModelingComponent]
    });
    fixture = TestBed.createComponent(ScaleModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
