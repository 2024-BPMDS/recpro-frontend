import {Component, Inject} from '@angular/core';
import {Rating} from "../../../../model/RecPro/rating/modelig/Rating";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {RatingModelingService} from "../service/rating-modeling.service";
import {RatingType} from "../../../../model/RecPro/rating/modelig/RatingType";
import {BinaryRating} from "../../../../model/RecPro/rating/modelig/BinaryRating";
import {ContinuousRating} from "../../../../model/RecPro/rating/modelig/ContinuousRating";
import {Scale} from "../../../../model/RecPro/rating/modelig/Scale";
import {IntervalBasedRating} from "../../../../model/RecPro/rating/modelig/IntervalBasedRating";
import {OrdinalRating} from "../../../../model/RecPro/rating/modelig/OrdinalRating";
import {UnaryRating} from "../../../../model/RecPro/rating/modelig/UnaryRating";

@Component({
  selector: 'app-rating-creator-dialog',
  templateUrl: './rating-creator-dialog.component.html',
  styleUrls: ['./rating-creator-dialog.component.css']
})
export class RatingCreatorDialogComponent {

  typeOptions = Object.values(RatingType);
  rating: Rating = new Rating();

  constructor(
    private dialogRef: MatDialogRef<RatingCreatorDialogComponent>,
    private ratingModelingService: RatingModelingService,
    @Inject(MAT_DIALOG_DATA) public ratingData: Rating
  ) {
    if (this.ratingData) {
      this.rating = this.ratingData;
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.ratingModelingService.create(this.rating);
    this.dialogRef.close();
  }

  getBinaryRating(): BinaryRating {
    return this.rating as BinaryRating;
  }

  getContinuousRating(): ContinuousRating {
    let res = this.rating as ContinuousRating;
    if (res.scale) {
      return res;
    }
    res.scale = new Scale();
    return res;
  }

  getIntervalBasedRating(): IntervalBasedRating {
    let res = this.rating as ContinuousRating;
    if (res.scale) {
      return res;
    } else {
      res.scale = new Scale();
      return res;
    }
  }

  getOrdinalRating(): OrdinalRating {
    let res = this.rating as OrdinalRating;
    if (res.scale) {
      return res;
    } else {
      res.scale = new Scale();
      return res;
    }
  }

  getUnaryRating(): UnaryRating {
    return this.rating as UnaryRating;
  }

  log() {
    console.log(this.rating);
  }

}
