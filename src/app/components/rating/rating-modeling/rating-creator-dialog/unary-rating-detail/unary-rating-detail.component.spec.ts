import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnaryRatingDetailComponent } from './unary-rating-detail.component';

describe('UnaryRatingDetailComponent', () => {
  let component: UnaryRatingDetailComponent;
  let fixture: ComponentFixture<UnaryRatingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UnaryRatingDetailComponent]
    });
    fixture = TestBed.createComponent(UnaryRatingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
