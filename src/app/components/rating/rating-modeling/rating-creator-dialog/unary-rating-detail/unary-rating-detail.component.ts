import {Component, Input} from '@angular/core';
import {UnaryRating} from "../../../../../model/RecPro/rating/modelig/UnaryRating";

@Component({
  selector: 'app-unary-rating-detail',
  templateUrl: './unary-rating-detail.component.html',
  styleUrls: ['./unary-rating-detail.component.css']
})
export class UnaryRatingDetailComponent {
  @Input() rating: UnaryRating = new UnaryRating();
}
