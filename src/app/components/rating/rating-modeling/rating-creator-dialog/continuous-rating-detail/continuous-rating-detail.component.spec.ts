import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinuousRatingDetailComponent } from './continuous-rating-detail.component';

describe('ContinuousRatingDetailComponent', () => {
  let component: ContinuousRatingDetailComponent;
  let fixture: ComponentFixture<ContinuousRatingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ContinuousRatingDetailComponent]
    });
    fixture = TestBed.createComponent(ContinuousRatingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
