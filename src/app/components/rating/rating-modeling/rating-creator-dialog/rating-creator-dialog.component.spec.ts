import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingCreatorDialogComponent } from './rating-creator-dialog.component';

describe('RatingCreatorDialogComponent', () => {
  let component: RatingCreatorDialogComponent;
  let fixture: ComponentFixture<RatingCreatorDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingCreatorDialogComponent]
    });
    fixture = TestBed.createComponent(RatingCreatorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
