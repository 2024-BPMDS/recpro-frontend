import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntervalBasedRatingDetailComponent } from './interval-based-rating-detail.component';

describe('IntervalBasedRatingDetailComponent', () => {
  let component: IntervalBasedRatingDetailComponent;
  let fixture: ComponentFixture<IntervalBasedRatingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IntervalBasedRatingDetailComponent]
    });
    fixture = TestBed.createComponent(IntervalBasedRatingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
