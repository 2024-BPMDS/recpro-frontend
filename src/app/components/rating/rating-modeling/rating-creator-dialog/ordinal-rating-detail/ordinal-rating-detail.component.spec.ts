import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdinalRatingDetailComponent } from './ordinal-rating-detail.component';

describe('OrdinalRatingDetailComponent', () => {
  let component: OrdinalRatingDetailComponent;
  let fixture: ComponentFixture<OrdinalRatingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrdinalRatingDetailComponent]
    });
    fixture = TestBed.createComponent(OrdinalRatingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
