import {Component, Input} from '@angular/core';
import {BinaryRating} from "../../../../../model/RecPro/rating/modelig/BinaryRating";

@Component({
  selector: 'app-binary-rating-detail',
  templateUrl: './binary-rating-detail.component.html',
  styleUrls: ['./binary-rating-detail.component.css']
})
export class BinaryRatingDetailComponent {

  @Input() rating: BinaryRating = new BinaryRating();

}
