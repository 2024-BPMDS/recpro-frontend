import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryRatingDetailComponent } from './binary-rating-detail.component';

describe('BinaryRatingDetailComponent', () => {
  let component: BinaryRatingDetailComponent;
  let fixture: ComponentFixture<BinaryRatingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BinaryRatingDetailComponent]
    });
    fixture = TestBed.createComponent(BinaryRatingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
