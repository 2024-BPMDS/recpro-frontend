import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../util/config.service";
import {BehaviorSubject, Observable} from "rxjs";
import {User} from "../../../model/User/User";
import {LoaderService} from "../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class UserModelingService {

  private users: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    private loaderService: LoaderService
  ) { }

  getAll(): Observable<User[]> {
    this.loaderService.load();
    this.http.get<User[]>(this.configService.apiConfig.modeling.user.getAll()).subscribe( res => {
      this.setUsers(res);
      this.loaderService.finish();
    });
    return this.getUsers();
  }

  create(user: User) {
    this.loaderService.load();
    this.http.post(this.configService.apiConfig.modeling.user.create(), user).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  setUsers(users: User[]) {
    this.users.next(users);
  }

  getUsers(): Observable<User[]> {
    return this.users.asObservable();
  }

  delete(userId: string) {
    this.loaderService.load();
    this.http.delete(this.configService.apiConfig.modeling.user.delete(userId)).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  getById(userId: string): Observable<User> {
    return this.http.get<User>(this.configService.apiConfig.modeling.user.getById(userId));
  }
}
