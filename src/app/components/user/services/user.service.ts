import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {User} from "../../../model/User/User";
import {UserModelingService} from "./user-modeling.service";
import {LoaderService} from "../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUser: BehaviorSubject<User> = new BehaviorSubject<User>(new User());

  constructor(
    private loaderService: LoaderService,
    private userModelingService: UserModelingService
  ) {
    let storageUserId = localStorage.getItem('userID');
    let userId = '31dea2b6-7de1-47e4-b5cb-ef4eda5549c1';

    console.log(storageUserId);
    console.log(userId);

    if (storageUserId != null && storageUserId !== '') {
      userId = storageUserId;
    }

    this.loaderService.load();
    this.userModelingService.getById(userId).subscribe(res => {
      this.loaderService.finish();
      this.setCurrentUser(res);
    });
  }

  setCurrentUser(user: User) {
    localStorage.setItem('userID', user.id);
    this.currentUser.next(user);
  }

  getCurrentUser(): Observable<User> {
    return this.currentUser.asObservable();
  }
}
