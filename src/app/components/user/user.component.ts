import {Component, OnInit} from '@angular/core';
import {User} from "../../model/User/User";
import {UserModelingService} from "./services/user-modeling.service";
import {MatDialog} from "@angular/material/dialog";
import {UserCreatorDialogComponent} from "./user-creator-dialog/user-creator-dialog.component";
import {UserService} from "./services/user.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users: User[] = [];

  currentUser: User = new User();

  constructor(
    private userModelingService: UserModelingService,
    private userService: UserService,
    private addUserDialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.userModelingService.getAll().subscribe(res => {
      this.users = res;
    });
  }

  addUser() {
    this.addUserDialog.open(
      UserCreatorDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        closeOnNavigation: false,
        disableClose: true,
        // minHeight: 800,
        maxHeight: 800,
        minWidth: 800,
        maxWidth: 800
      }
    );
  }

  deleteUser(user: User) {
    this.userModelingService.delete(user.id);
  }

  editUser(user: User) {
    this.addUserDialog.open(
      UserCreatorDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        closeOnNavigation: false,
        disableClose: true,
        // minHeight: 800,
        maxHeight: 800,
        minWidth: 800,
        maxWidth: 800,
        data: user
      }
    );
  }

  setCurrentUser(user: User) {
    this.userService.setCurrentUser(user);
  }
}
