import { Component } from '@angular/core';
import {AttributeAdminService} from "../service/attribute-admin.service";

@Component({
  selector: 'app-create-attribute',
  templateUrl: './create-attribute.component.html',
  styleUrls: ['./create-attribute.component.css']
})
export class CreateAttributeComponent {

  constructor(
    private attributeAdminService: AttributeAdminService
  ) {
  }

  create() {
  }

  getAttributes() {
    this.attributeAdminService.getAttributes();
  }
}
