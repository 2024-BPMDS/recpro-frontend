import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAttributeComponent } from './create-attribute/create-attribute.component';



@NgModule({
  declarations: [
    CreateAttributeComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AttributeAdminModule { }
