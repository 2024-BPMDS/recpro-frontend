import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AttributeAdminService {

  constructor(
    private http: HttpClient
  ) { }

  getAttributes() {
    this.http.get('http://localhost:8080/api/ontology/attribute/getAttributes').subscribe(res => {
      console.log(res);
    });
  }
}
