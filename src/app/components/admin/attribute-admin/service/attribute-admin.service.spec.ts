import { TestBed } from '@angular/core/testing';

import { AttributeAdminService } from './attribute-admin.service';

describe('AttributeAdminService', () => {
  let service: AttributeAdminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AttributeAdminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
