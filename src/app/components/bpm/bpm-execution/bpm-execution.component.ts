import {Component, OnInit} from '@angular/core';
import {TasklistService} from "./services/tasklist.service";
import {TasklistOrder} from "../../../model/BPM/execution/TasklistOrder";
import {RecproTasklistDto} from "../../../model/BPM/execution/RecproTasklistDto";
import {RecproTaskDto} from "../../../model/BPM/execution/RecproTaskDto";
import {MatDialog} from "@angular/material/dialog";
import {FilterDialogComponent} from "./filter-dialog/filter-dialog.component";
import {TaskService} from "./services/task.service";
import {RecproFilterInstance} from "./filter-dialog/model/RecproFilterInstance";

@Component({
  selector: 'app-bpm-execution',
  templateUrl: './bpm-execution.component.html',
  styleUrls: ['./bpm-execution.component.css']
})
export class BpmExecutionComponent implements OnInit{

  tasklist: RecproTasklistDto = new RecproTasklistDto();
  currentTask: RecproTaskDto = new RecproTaskDto();

  orderByOptions = Object.values(TasklistOrder);

  constructor(
    private tasklistService: TasklistService,
    private filterDialog: MatDialog,
    private taskService: TaskService
  ) {
  }
  ngOnInit(
  ) {
    this.taskService.setCurrentTask(new RecproTaskDto());
    this.tasklistService.getTasklist().subscribe(res => {
      console.log(res);
      this.tasklist = res;
    });
    this.taskService.getCurrentTask().subscribe(res => {
      this.currentTask = res;
    });
  }

  openFilter() {
    const dialogRef = this.filterDialog.open(
      FilterDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        closeOnNavigation: false,
        disableClose: true,
        // minHeight: 800,
        minWidth: 800,
        maxWidth: 800,
        maxHeight: 820,
        data: this.tasklist
      }
    );

    dialogRef.afterClosed().subscribe(res => {
      if (res === 'save') {
        this.saveFilter(this.tasklist.knowledgeBasedFilter.filterInstance);
      } else {
        this.updateTasklist();
      }
    });
  }

  setReverse() {
    this.tasklist.baseFilter.filterInstance.ascending = !this.tasklist.baseFilter.filterInstance.ascending;
    this.saveFilter(this.tasklist.baseFilter.filterInstance);
  }

  saveFilter(filterInstance: RecproFilterInstance) {
    this.tasklistService.filterTasklist(filterInstance);
  }

  updateTasklist() {
    this.tasklistService.getTasklist();
  }

  resetTask() {
    this.taskService.setCurrentTask(new RecproTaskDto());
  }
}
