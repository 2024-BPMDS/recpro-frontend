import { Component } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {StartProcessDetailComponent} from "./start-process-detail/start-process-detail.component";

@Component({
  selector: 'app-start-process',
  templateUrl: './start-process.component.html',
  styleUrls: ['./start-process.component.css']
})
export class StartProcessComponent {

  constructor(
    public dialog: MatDialog
  ) {
  }
  startProcess() {
    const dialogRef = this.dialog.open(StartProcessDetailComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        closeOnNavigation: false,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      console.log('Start Process closed');
    });
  }

}
