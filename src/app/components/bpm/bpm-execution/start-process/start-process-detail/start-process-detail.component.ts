import {Component, OnInit} from '@angular/core';
import {ProcessService} from "../../../bpm-modeling/service/process.service";
import {RecproProcess} from "../../../../../model/BPM/modeling/RecproProcess";
import {MatDialogRef} from "@angular/material/dialog";
import {ProcessExecutionService} from "../../services/process-execution.service";
import {TasklistService} from "../../services/tasklist.service";
import {LoaderService} from "../../../../../util/service/loader.service";

@Component({
  selector: 'app-start-process-detail',
  templateUrl: './start-process-detail.component.html',
  styleUrls: ['./start-process-detail.component.css']
})
export class StartProcessDetailComponent implements OnInit{

  processes: RecproProcess[] = [];

  constructor(
    private processService: ProcessService,
    private processExecutionService: ProcessExecutionService,
    private tasklistService: TasklistService,
    private dialogRef: MatDialogRef<StartProcessDetailComponent>,
    private loaderService: LoaderService
  ) {
  }

  ngOnInit() {
    this.loaderService.load();
    this.processService.getLatest().subscribe(res => {
      this.processes = res;
      this.loaderService.finish();
    });
  }

  startProcess(process: RecproProcess) {
    this.loaderService.load();
    this.processExecutionService.start(process.id).subscribe(() => {
      this.tasklistService.getTasklist();
      this.loaderService.finish();
      this.dialogRef.close();
    });
  }

  cancel() {
    this.dialogRef.close();
  }

}
