import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartProcessDetailComponent } from './start-process-detail.component';

describe('StartProcessDetailComponent', () => {
  let component: StartProcessDetailComponent;
  let fixture: ComponentFixture<StartProcessDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StartProcessDetailComponent]
    });
    fixture = TestBed.createComponent(StartProcessDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
