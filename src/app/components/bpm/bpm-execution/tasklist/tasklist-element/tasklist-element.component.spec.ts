import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TasklistElementComponent } from './tasklist-element.component';

describe('TasklistElementComponent', () => {
  let component: TasklistElementComponent;
  let fixture: ComponentFixture<TasklistElementComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TasklistElementComponent]
    });
    fixture = TestBed.createComponent(TasklistElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
