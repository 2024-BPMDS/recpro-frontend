import {Component, Input} from '@angular/core';
import {RecproTaskDto} from "../../../../../model/BPM/execution/RecproTaskDto";

@Component({
  selector: 'app-tasklist-element',
  templateUrl: './tasklist-element.component.html',
  styleUrls: ['./tasklist-element.component.css']
})
export class TasklistElementComponent {

  @Input() task: RecproTaskDto = new RecproTaskDto();
  @Input() color: string = 'white';
}
