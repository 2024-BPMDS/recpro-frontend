import {Component,Input} from '@angular/core';
import {RecproTasklistDto} from "../../../../model/BPM/execution/RecproTasklistDto";
import {RecproTaskDto} from "../../../../model/BPM/execution/RecproTaskDto";
import {TaskService} from "../services/task.service";

@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent {
  @Input() tasklist: RecproTasklistDto = new RecproTasklistDto();
  @Input() currentTask: RecproTaskDto = new RecproTaskDto();

  constructor(
    private taskService: TaskService
  ) {
    this.taskService.getCurrentTask().subscribe(res => this.currentTask = res);
  }

  selectTask(value: RecproTaskDto) {
    this.taskService.setCurrentTask(value);
  }

  getColor(task: RecproTaskDto) {
    if (this.currentTask == task) {
      return '#b4e1de';
    } else {
      return 'white';
    }
  }
}
