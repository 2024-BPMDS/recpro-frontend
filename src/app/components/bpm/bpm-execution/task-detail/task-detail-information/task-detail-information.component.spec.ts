import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskDetailInformationComponent } from './task-detail-information.component';

describe('TaskDetailInformationComponent', () => {
  let component: TaskDetailInformationComponent;
  let fixture: ComponentFixture<TaskDetailInformationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TaskDetailInformationComponent]
    });
    fixture = TestBed.createComponent(TaskDetailInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
