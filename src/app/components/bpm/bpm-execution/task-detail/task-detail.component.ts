import {Component, OnInit, ViewChild} from '@angular/core';
import {ClrLoadingState} from "@clr/angular";
import {AttributeService} from "../../../attribute/service/attribute.service";
import {TaskDetailFormComponent} from "./task-detail-form/task-detail-form.component";
import {AlertComponent} from "../../../../layout/alert/alert.component";
import {TaskService} from "../services/task.service";
import {UserService} from "../../../user/services/user.service";
import {RecproTaskDto} from "../../../../model/BPM/execution/RecproTaskDto";
import {RecproTaskState} from "../../../../model/BPM/execution/RecproTaskState";
import {MatDialog} from "@angular/material/dialog";
import {
  RatingExecutionDialogComponent
} from "../../../rating/rating-execution/rating-execution-dialog/rating-execution-dialog.component";
import {User} from "../../../../model/User/User";

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit{

  validateBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
   currentTask: RecproTaskDto = new RecproTaskDto();
   currentUser: User = new User();
  @ViewChild(TaskDetailFormComponent, {static: true}) formComponent: TaskDetailFormComponent | undefined;
  @ViewChild(AlertComponent, {static: true}) alertComponent: AlertComponent = new AlertComponent();

  constructor(
    private attributeService: AttributeService,
    private taskService: TaskService,
    public userService: UserService,
    private ratingExecutionDialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.taskService.getCurrentTask().subscribe(res => {
      this.currentTask = res;
    });

    this.userService.getCurrentUser().subscribe(res => this.currentUser = res);
  }

  saveAttributes() {
    if (this.formComponent) {
      this.attributeService.saveAttributes(this.currentTask.attributes).subscribe(() => {});
    }
  }

  startTask() {
    this.taskService.startTask(this.currentTask);
  }

  stopTask() {
    this.taskService.stopTask(this.currentTask);
  }

  isCurrentUserAssignee(): boolean {
    return this.currentTask.assigneeId === this.currentUser.id;
  }

  isTaskStarted(): boolean {
    return this.currentTask.state === RecproTaskState.IN_PROGRESS;
  }

  completeTask() {

    let dialogRef = this.ratingExecutionDialog.open(
      RatingExecutionDialogComponent,
      {
        enterAnimationDuration: 200,
        exitAnimationDuration: 200,
        data: this.currentTask,
        // minHeight: 800,
        // minWidth: 800,
        maxHeight: 800,
        maxWidth: 800,
        closeOnNavigation: false,
        disableClose: true
      }
    );

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.taskService.completeTask(this.currentTask);
      }
    });
  }

  unclaim() {
    this.taskService.unclaimTask(this.currentTask);
  }

  claim() {
    this.taskService.claimTask(this.currentTask);
  }

  log() {
    console.log(this.currentTask);
  }
}
