import {Component, OnInit} from '@angular/core';
import {RecproTaskDto} from "../../../../../model/BPM/execution/RecproTaskDto";
import {UserService} from "../../../../user/services/user.service";
import {RecproTaskState} from "../../../../../model/BPM/execution/RecproTaskState";
import {TaskService} from "../../services/task.service";
import {User} from "../../../../../model/User/User";

@Component({
  selector: 'app-task-detail-form',
  templateUrl: './task-detail-form.component.html',
  styleUrls: ['./task-detail-form.component.css']
})
export class TaskDetailFormComponent implements OnInit{

    currentTask: RecproTaskDto = new RecproTaskDto();
    currentUser: User = new User();

  constructor(
    private userService: UserService,
    private taskService: TaskService
  ) {
  }

  ngOnInit() {
    this.taskService.getCurrentTask().subscribe(res => {
      this.currentTask = res;
    });

    this.userService.getCurrentUser().subscribe(res => this.currentUser = res);
  }

  isCurrentUserAssignee(): boolean {
    return this.currentTask.assigneeId === this.currentUser.id;
  }

  isTaskStarted(): boolean {
    return this.currentTask.state === RecproTaskState.IN_PROGRESS;
  }
}
