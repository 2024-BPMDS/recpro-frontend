import { Component } from '@angular/core';
import {TaskDetailComponent} from "../task-detail.component";

@Component({
  selector: 'app-task-detail-process',
  templateUrl: './task-detail-process.component.html',
  styleUrls: ['./task-detail-process.component.css']
})
export class TaskDetailProcessComponent extends TaskDetailComponent {

}
