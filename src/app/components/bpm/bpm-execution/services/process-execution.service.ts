import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProcessExecutionService {

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService
  ) { }

  start(processId: string): Observable<any> {
    return this.httpClient.post(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.process.start(processId), undefined);
  }
}
