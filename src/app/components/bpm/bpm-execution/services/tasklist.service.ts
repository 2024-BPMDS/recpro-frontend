import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {BehaviorSubject, Observable, share} from "rxjs";
import {TasklistOrder} from "../../../../model/BPM/execution/TasklistOrder";
import {RecproTasklistDto} from "../../../../model/BPM/execution/RecproTasklistDto";
import {RecproFilterInstance} from "../filter-dialog/model/RecproFilterInstance";
import {LoaderService} from "../../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class TasklistService {

  private tasklist: BehaviorSubject<RecproTasklistDto> = new BehaviorSubject<RecproTasklistDto>(new RecproTasklistDto());

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    private loaderService: LoaderService
  ) { }

  getTasklistByAssignee(orderBy: TasklistOrder, ascending: boolean): Observable<RecproTasklistDto>{
    return this.http.get<RecproTasklistDto>( this.configService.apiConfig.recpro.bpm.bpmExecutionApi.tasklist.getTasklistByAssignee(orderBy, ascending));
  }

  getTasklist(): Observable<RecproTasklistDto> {
    this.loaderService.load();
    this.http.get<RecproTasklistDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.tasklist.getAllTasks()).subscribe(res => {
      res.tasks.sort((a, b) => a.position - b.position);
      console.log(res);
      this.setTasklist(res);
      this.loaderService.finish();
    });
    return this.getTaskListObservable();
  }

  getTaskListObservable(): Observable<RecproTasklistDto> {
    return this.tasklist.asObservable().pipe(share());
  }

  filterTasklist(filterInstance: RecproFilterInstance): Observable<RecproTasklistDto> {
    this.loaderService.load();
    this.http.post<RecproTasklistDto>(this.configService.apiConfig.recpro.bpm.bpmExecutionApi.tasklist.filterTasklist(), filterInstance).subscribe(res => {
      res.tasks.sort((a, b) => a.position - b.position);
      this.setTasklist(res);
      this.loaderService.finish();
    });
    return this.getTaskListObservable();
  }

  setTasklist(tasklist: RecproTasklistDto) {
    this.tasklist.next(tasklist);
  }
}
