import { TestBed } from '@angular/core/testing';

import { ProcessExecutionService } from './process-execution.service';

describe('ProcessExecutionService', () => {
  let service: ProcessExecutionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcessExecutionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
