import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterExecutionBpmElementDetailComponent } from './filter-execution-bpm-element-detail.component';

describe('FilterExecutionBpmElementDetailComponent', () => {
  let component: FilterExecutionBpmElementDetailComponent;
  let fixture: ComponentFixture<FilterExecutionBpmElementDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterExecutionBpmElementDetailComponent]
    });
    fixture = TestBed.createComponent(FilterExecutionBpmElementDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
