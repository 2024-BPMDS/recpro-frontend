import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {
  KnowledgeBasedFilterModelingService
} from "../../../filter/filter-modeling/service/knowledge-based-filter-modeling.service";
import {RecproKnowledgeBasedFilter} from "../../../../model/RecPro/filter/RecproKnowledgeBasedFilter";
import {RecproTasklistDto} from "../../../../model/BPM/execution/RecproTasklistDto";
import {
  KnowledgeBasedFilterExecutionService
} from "../../../filter/filter-execution/service/knowledge-based-filter-execution.service";
import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproElement} from "../../../../model/BPM/modeling/RecproElement";
import {MatSelectChange} from "@angular/material/select";
import {RecproFilterAttributeInstance} from "./model/attribute/RecproFilterAttributeInstance";
import {RecproKnowledgeBasedFilterInstance} from "./model/RecproKnowledgeBasedFilterInstance";
import {RecproFilterRecproElementInstance} from "./model/RecproFilterRecproElementInstance";
import {TasklistService} from "../services/tasklist.service";
import {FilterType} from "../../../../model/RecPro/filter/FilterType";

@Component({
  selector: 'app-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.css']
})
export class FilterDialogComponent implements OnInit{

  filters: RecproKnowledgeBasedFilter[] = [];
  tasklist: RecproTasklistDto = new RecproTasklistDto();
  constructor(
    private dialogRef: MatDialogRef<FilterDialogComponent>,
    private filterModelingService: KnowledgeBasedFilterModelingService,
    private filterExecutionService: KnowledgeBasedFilterExecutionService,
    private tasklistService: TasklistService,
    // @Inject(MAT_DIALOG_DATA) public tasklist: RecproTasklistDto,
  ) {
  }
  ngOnInit() {
    this.filterModelingService.getAll().subscribe(res => {
      this.filters = res;
    });
    this.tasklistService.getTaskListObservable().subscribe(res => {
      this.tasklist = res;
      console.log('ON INIT:');
      console.log(res);
    });
  }

  cancel() {
    this.dialogRef.close('cancel');
  }

  save() {
    this.dialogRef.close('save');
  }

  updateFilter(event: MatSelectChange) {
    this.filterExecutionService.createByFilterId(event.value, this.tasklist.id, FilterType.KNOWLEDGE_BASED).subscribe(res => {
      this.tasklist.knowledgeBasedFilter = res;

      // this.tasklistService.setTasklist(this.tasklist);
      console.log('UPDATE FILTER');
      console.log(res);
    });
  }

  getAttributes(): RecproAttribute[] {
    if (this.tasklist.knowledgeBasedFilter.filter.id !== '') {
      return (this.tasklist.knowledgeBasedFilter.filter as RecproKnowledgeBasedFilter).attributes;
    } else {
      return [];
    }
  }

  getRecproElements(): RecproElement[] {
    if (this.tasklist.knowledgeBasedFilter.filter.id !== '') {
      return (this.tasklist.knowledgeBasedFilter.filter as RecproKnowledgeBasedFilter).bpmElements;
    } else {
      return [];
    }
  }

  getAttributeInstances(): RecproFilterAttributeInstance[] {
    return (this.tasklist.knowledgeBasedFilter.filterInstance as RecproKnowledgeBasedFilterInstance).attributes;
  }

  getRecproElementInstances(): RecproFilterRecproElementInstance[] {
    return (this.tasklist.knowledgeBasedFilter.filterInstance as RecproKnowledgeBasedFilterInstance).elements;
  }

  log() {
    console.log(this.filters);
    console.log(this.tasklist);
    console.log(this.getAttributes());
    console.log(this.getRecproElements());
  }
}
