import { TestBed } from '@angular/core/testing';

import { FilterDialogService } from './filter-dialog.service';

describe('FilterDialogService', () => {
  let service: FilterDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilterDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
