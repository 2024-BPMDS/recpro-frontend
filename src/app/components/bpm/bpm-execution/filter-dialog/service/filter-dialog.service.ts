import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {RecproAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproFilterAttributeInstance} from "../model/attribute/RecproFilterAttributeInstance";
import {RecproElement} from "../../../../../model/BPM/modeling/RecproElement";
import {RecproFilterRecproElementInstance} from "../model/RecproFilterRecproElementInstance";

@Injectable({
  providedIn: 'root'
})
export class FilterDialogService {

  private attributes: BehaviorSubject<RecproAttribute[]> = new BehaviorSubject<RecproAttribute[]>([]);
  private attributeInstances: BehaviorSubject<RecproFilterAttributeInstance[]> = new BehaviorSubject<RecproFilterAttributeInstance[]>([]);

  private bpmElements: BehaviorSubject<RecproElement[]> = new BehaviorSubject<RecproElement[]>([]);
  private bpmElementInstances: BehaviorSubject<RecproFilterRecproElementInstance[]> = new BehaviorSubject<RecproFilterRecproElementInstance[]>([]);

  constructor() { }

  getAttributes(): Observable<RecproAttribute[]> {
    return this.attributes;
  }

  setAttributes(attributes: RecproAttribute[]) {
    this.attributes.next(attributes);
  }

  getAttributeInstances(): Observable<RecproFilterAttributeInstance[]> {
    return this.attributeInstances;
  }

  setAttributeInstances(attributeInstances: RecproFilterAttributeInstance[]) {
    this.attributeInstances.next(attributeInstances);
  }

  getBpmElements(): Observable<RecproElement[]> {
    return this.bpmElements;
  }

  setBpmElements(bpmElements: RecproElement[]) {
    this.bpmElements.next(bpmElements);
  }

  getBpmElementInstances(): Observable<RecproFilterRecproElementInstance[]> {
    return this.bpmElementInstances;
  }

  setBpmElementInstances(bpmElementInstances: RecproFilterRecproElementInstance[]) {
    this.bpmElementInstances.next(bpmElementInstances);
  }
}
