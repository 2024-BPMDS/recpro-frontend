import {RecproFilterInstance} from "./RecproFilterInstance";
import {RecproFilterAttributeInstance} from "./attribute/RecproFilterAttributeInstance";
import {RecproFilterRecproElementInstance} from "./RecproFilterRecproElementInstance";
import {FilterBpmElementInstanceInstance} from "./bpm-element-instance/FilterBpmElementInstanceInstance";

export class RecproKnowledgeBasedFilterInstance extends RecproFilterInstance {
  attributes: RecproFilterAttributeInstance[] = [];
  elements: RecproFilterRecproElementInstance[] = [];
  elementInstances: FilterBpmElementInstanceInstance[] = [];

  constructor() {
    super();
  }

}
