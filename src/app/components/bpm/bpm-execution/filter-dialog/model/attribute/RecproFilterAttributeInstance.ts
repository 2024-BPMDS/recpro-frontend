import {RecproAttributeType} from "../../../../../../model/RecPro/attributes/modeling/RecproAttributeType";
import {RecproFilterAttributeState} from "./RecproFilterAttributeState";

export class RecproFilterAttributeInstance {
  id: number;
  attributeId: string;
  active: boolean;
  attributeType: RecproAttributeType;
  state: RecproFilterAttributeState;

  constructor() {
    this.id = -1;
    this.attributeId = '';
    this.active = false;
    this.attributeType = RecproAttributeType.META;
    this.state =RecproFilterAttributeState.NEUTRAL;
  }

}
