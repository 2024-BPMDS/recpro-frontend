export class FilterResultItem {

  id: number;
  taskId: number;
  value: number;
  position: number;
  description: string;
  visible: boolean;

  constructor() {
    this.id = -1;
    this.taskId = -1;
    this.value = -1;
    this.position = -1;
    this.description = '';
    this.visible = true;
  }

}
