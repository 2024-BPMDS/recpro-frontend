import {FilterResultItem} from "./FilterResultItem";

export class FilterResult {
  id: number;
  items: FilterResultItem[] = [];

  constructor() {
    this.id = -1;
    this.items = [];
  }
}
