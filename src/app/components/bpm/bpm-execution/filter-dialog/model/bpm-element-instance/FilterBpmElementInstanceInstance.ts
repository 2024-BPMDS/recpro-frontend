import {FilterBpmElementInstanceState} from "./FilterBpmElementInstanceState";

export class FilterBpmElementInstanceInstance {
  id: number;
  recproElementInstanceId: string;
  state: FilterBpmElementInstanceState;

  constructor() {
    this.id = -1;
    this.recproElementInstanceId = '';
    this.state = FilterBpmElementInstanceState.NEUTRAL;
  }
}
