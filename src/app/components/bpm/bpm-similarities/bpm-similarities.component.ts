import {Component, OnInit} from '@angular/core';
import {TaskService} from "../bpm-execution/services/task.service";
import {RecproTaskDto} from "../../../model/BPM/execution/RecproTaskDto";
import {HttpClient} from "@angular/common/http";
import {RecproFilterInstanceDto} from "../bpm-execution/filter-dialog/model/RecproFilterInstanceDto";
import {
  RecproKnowledgeBasedFilterInstance
} from "../bpm-execution/filter-dialog/model/RecproKnowledgeBasedFilterInstance";
import {FilterResultItem} from "../bpm-execution/filter-dialog/model/result/FilterResultItem";
import {TasklistService} from "../bpm-execution/services/tasklist.service";
import {RecproTasklistDto} from "../../../model/BPM/execution/RecproTasklistDto";
import {RecproTaskState} from "../../../model/BPM/execution/RecproTaskState";
import {RecproActivity} from "../../../model/BPM/modeling/RecproActivity";
import {RecproProcessInstanceDto} from "../../../model/BPM/execution/RecproProcessInstanceDto";

@Component({
  selector: 'app-bpm-similarities',
  templateUrl: './bpm-similarities.component.html',
  styleUrls: ['./bpm-similarities.component.css']
})
export class BpmSimilaritiesComponent implements OnInit {

  latestExecuted: RecproTaskDto = new RecproTaskDto();

  belongingToProcessInstance: FilterResultItem[] = [];
  belongingToProcessInstanceTasks: RecproTaskDto[] = [];

  tasklist: RecproTasklistDto = new RecproTasklistDto();

  similarTasks: RecproTaskDto[] = [];

  constructor(
    private taskService: TaskService,
    private httpClient: HttpClient,
    private tasklistService: TasklistService
  ) {
  }

  ngOnInit() {
    this.taskService.getLatestExecuted().subscribe(res => {
      // this.latestExecuted = res;
      this.latestExecuted = new RecproTaskDto();
      this.latestExecuted.activity.name = 'Analysis and identification of causes';
      this.latestExecuted.processInstance.processName = 'Customer Complaints Management';
      this.latestExecuted.priority = 50;
      this.latestExecuted.createDate = new Date('1/14/24, 3:51');
      this.latestExecuted.assigneeId = '1';
    });

    this.tasklistService.getTaskListObservable().subscribe(res => {
      this.tasklist = res;
      // this.test();
    });

    this.continueInstance();
    this.similarTasksFunction();
  }

  continueInstance() {
    let t = new RecproTaskDto();
    t.activity.name = 'Action plan development';
    t.processInstance.processName = 'Customer Complaints Management';
    t.priority = 50;
    t.createDate = new Date('1/14/24, 3:52');
    t.assigneeId = '-1';
    this.belongingToProcessInstanceTasks.push(t);
  }

  similarTasksFunction() {
    let t = new RecproTaskDto();
    t.activity.name = 'Complaint recording';
    t.processInstance.processName = 'Customer Complaints Management';
    t.priority = 50;
    t.createDate = new Date('1/14/24, 3:52');
    t.assigneeId = '-1';
    this.similarTasks.push(t);
    let t2 = new RecproTaskDto();
    t2.activity.name = 'Create roster';
    t2.processInstance.processName = 'Technician service management';
    t2.priority = 50;
    t2.createDate = new Date('1/14/24, 3:34 PM');
    t2.assigneeId = '-1';
    this.similarTasks.push(t2);
  }

  test() {
    this.httpClient.post<RecproFilterInstanceDto>('http://localhost:8080/api/recpro/filter/execution/similarActivity/processInstance', this.latestExecuted).subscribe(similars => {
      console.log(similars);
      this.belongingToProcessInstance = (similars.filterInstance as RecproKnowledgeBasedFilterInstance).result.items;
    })
  }

  getTaskById(id: number): RecproTaskDto {
    let res = this.tasklist.tasks.find(task => task.id === id);
    if (res) {
      return res;
    } else {
      return new RecproTaskDto();
    }
  }

}
