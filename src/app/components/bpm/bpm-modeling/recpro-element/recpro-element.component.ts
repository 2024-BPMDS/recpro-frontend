import { Component } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {AddAttributeComponent} from "../../../attribute/attribute-modeling/add-attribute/add-attribute.component";
import {ProcessService} from "../service/process.service";

@Component({
  selector: 'app-recpro-element',
  templateUrl: './recpro-element.component.html',
  styleUrls: ['./recpro-element.component.css']
})
export class RecproElementComponent {

  constructor(
    public dialog: MatDialog,
  public processService: ProcessService,
  ) {
  }
  addAttribute(elementId: string) {
    console.log('ADD: ' + elementId);
    const dialogRef = this.dialog.open(AddAttributeComponent, {
      enterAnimationDuration: 200,
      exitAnimationDuration: 200,
      data: elementId,
      closeOnNavigation: false,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Dialog geschlossen');
    });
  }

  addRating(elementId: string) {

  }
}
