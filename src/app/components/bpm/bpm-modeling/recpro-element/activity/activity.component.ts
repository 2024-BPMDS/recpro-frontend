import {Component, Input, OnInit} from '@angular/core';
import {RecproActivity} from "../../../../../model/BPM/modeling/RecproActivity";
import {RecproElementComponent} from "../recpro-element.component";

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent extends RecproElementComponent implements OnInit{

  @Input() activities: RecproActivity[] = [];

  ngOnInit() {
  }
}
