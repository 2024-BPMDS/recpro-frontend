import { TestBed } from '@angular/core/testing';

import { RecproElementService } from './recpro-element.service';

describe('RecproElementService', () => {
  let service: RecproElementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecproElementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
