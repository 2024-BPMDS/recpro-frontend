import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {Observable} from "rxjs";
import {RecproElement} from "../../../../model/BPM/modeling/RecproElement";

@Injectable({
  providedIn: 'root'
})
export class RecproElementService {

  constructor(
    private httpClient: HttpClient,
    private config: ConfigService
  ) { }

  getAll(): Observable<RecproElement[]> {
    return this.httpClient.get<RecproElement[]>(this.config.apiConfig.modeling.bpm.bpmElement.getAll());
  }
}
