import {Component, Input, OnInit} from '@angular/core';
import {RecproAttributeInstance} from "../../model/RecPro/attributes/execution/RecproAttributeInstance";

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.css']
})
export class AttributeComponent implements OnInit {

  @Input() attribute: RecproAttributeInstance = new RecproAttributeInstance();

  ngOnInit() {
    console.log(this.attribute);
  }

}
