import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {RecproAttributeType} from "../../../../model/RecPro/attributes/modeling/RecproAttributeType";
import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproTextualAttribute} from "../../../../model/RecPro/attributes/modeling/RecproTextualAttribute";
import {RecproBinaryAttribute} from "../../../../model/RecPro/attributes/modeling/RecproBinaryAttribute";
import {RecproMetaAttribute} from "../../../../model/RecPro/attributes/modeling/RecproMetaAttribute";
import {RecproNumericAttribute} from "../../../../model/RecPro/attributes/modeling/RecproNumericAttribute";
import {AttributeModelingService} from "../service/attribute-modeling.service";

@Component({
  selector: 'app-add-attribute',
  templateUrl: './add-attribute.component.html',
  styleUrls: ['./add-attribute.component.css']
})
export class AddAttributeComponent implements OnInit{
  typeOptions = Object.values(RecproAttributeType);
  attribute: RecproAttribute = new RecproAttribute();
  attributes: RecproAttribute[] = [];
  createOrSelect: string = 'select';

  constructor(
    private dialogRef: MatDialogRef<AddAttributeComponent>,
    @Inject(MAT_DIALOG_DATA) public elementId: string,
    private attributeModelingService: AttributeModelingService) {
  }

  ngOnInit() {
    this.attributeModelingService.getAll().subscribe(res => {
      this.attributes = res;
    });
  }


  getTextualAttribute(): RecproTextualAttribute {
    return this.attribute as RecproTextualAttribute;
  }

  getBinaryAttribute(): RecproBinaryAttribute {
    return this.attribute as RecproBinaryAttribute;
  }

  getMetaAttribute(): RecproMetaAttribute {
    return this.attribute as RecproMetaAttribute;
  }

  getNumericAttribute(): RecproNumericAttribute {
    return this.attribute as RecproNumericAttribute;
  }

  save() {
    console.log(this.attribute);
    this.attributeModelingService.createWithElement(this.elementId, this.attribute);
  }

  clearAttribute() {
    this.attribute = new RecproAttribute();
  }

  cancel() {
    this.dialogRef.close();
  }

}
