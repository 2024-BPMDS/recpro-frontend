import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeModelingComponent } from './attribute-modeling.component';

describe('AttributeModelingComponent', () => {
  let component: AttributeModelingComponent;
  let fixture: ComponentFixture<AttributeModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeModelingComponent]
    });
    fixture = TestBed.createComponent(AttributeModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
