import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeCreatorDialogComponent } from './attribute-creator-dialog.component';

describe('AttributeCreatorDialogComponent', () => {
  let component: AttributeCreatorDialogComponent;
  let fixture: ComponentFixture<AttributeCreatorDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeCreatorDialogComponent]
    });
    fixture = TestBed.createComponent(AttributeCreatorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
