import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {BehaviorSubject, Observable} from "rxjs";
import {RecproElement} from "../../../../model/BPM/modeling/RecproElement";
import {LoaderService} from "../../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class AttributeModelingService {

  private attributes: BehaviorSubject<RecproAttribute[]> = new BehaviorSubject<RecproAttribute[]>([]);

  constructor(
    private http: HttpClient,
    private config: ConfigService,
    private loaderService: LoaderService
  ) { }

  create(attribute: RecproAttribute) {
    this.loaderService.load();
    this.http.post<RecproAttribute>(this.config.apiConfig.modeling.attribute.create(), attribute).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  createWithElement(elementId: string, attribute: RecproAttribute) {
    this.loaderService.load();
    this.http.post<RecproAttribute>(this.config.apiConfig.modeling.attribute.createWithElement(elementId), attribute).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  createWithElements(elements: RecproElement[], attribute: RecproAttribute) {
    this.loaderService.load();
    this.http.post<RecproAttribute>(this.config.apiConfig.modeling.attribute.createWithElements(elements.map(ele => ele.id)), attribute).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  getAll(): Observable<RecproAttribute[]> {
    this.loaderService.load();
    this.http.get<RecproAttribute[]>(this.config.apiConfig.modeling.attribute.getAll()).subscribe(res => {
      this.set(res);
      this.loaderService.finish();
    });
    return this.get();
  }

  delete(id: string) {
    this.loaderService.load();
    this.http.delete(this.config.apiConfig.modeling.attribute.delete(id)).subscribe(() => {
      this.getAll();
      this.loaderService.finish();
    });
  }

  set(attributes: RecproAttribute[]) {
    this.attributes.next(attributes);
  }

  get(): Observable<RecproAttribute[]> {
    return this.attributes.asObservable();
  }
}
