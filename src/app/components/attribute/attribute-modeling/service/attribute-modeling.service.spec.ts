import { TestBed } from '@angular/core/testing';

import { AttributeModelingService } from './attribute-modeling.service';

describe('AttributeModelingService', () => {
  let service: AttributeModelingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AttributeModelingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
