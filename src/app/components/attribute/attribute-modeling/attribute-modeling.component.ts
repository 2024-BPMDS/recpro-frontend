import {Component, Input, OnInit} from '@angular/core';
import {RecproAttribute} from "../../../model/RecPro/attributes/modeling/RecproAttribute";
import {MatDialog} from "@angular/material/dialog";
import {AttributeCreatorDialogComponent} from "./attribute-creator-dialog/attribute-creator-dialog.component";
import {AttributeModelingService} from "./service/attribute-modeling.service";

@Component({
  selector: 'app-attribute-modeling',
  templateUrl: './attribute-modeling.component.html',
  styleUrls: ['./attribute-modeling.component.css']
})
export class AttributeModelingComponent implements OnInit{

  @Input() attributes: RecproAttribute[] = [];

  constructor(
    private dialog: MatDialog,
    private modelingService: AttributeModelingService
  ) {
  }

  ngOnInit() {
    this.modelingService.getAll().subscribe(res => {
      console.log('GET ALL');
      this.attributes = res;
    });
  }

  addAttribute() {
    this.dialog.open(AttributeCreatorDialogComponent, {
      enterAnimationDuration: 200,
      exitAnimationDuration: 200,
      closeOnNavigation: false,
      disableClose: true,
    });
  }

  delete(attribute: RecproAttribute) {
    this.modelingService.delete(attribute.id);
  }

  edit(attribute: RecproAttribute) {
    this.dialog.open(AttributeCreatorDialogComponent, {
      enterAnimationDuration: 200,
      exitAnimationDuration: 200,
      data: attribute,
      closeOnNavigation: false,
      disableClose: true,
    });
  }
}
