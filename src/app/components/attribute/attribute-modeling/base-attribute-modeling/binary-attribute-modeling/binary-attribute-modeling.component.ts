import {Component, Input} from '@angular/core';
import {BaseAttributeModelingComponent} from "../base-attribute-modeling.component";
import {RecproBinaryAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproBinaryAttribute";

@Component({
  selector: 'app-binary-attribute-modeling',
  templateUrl: './binary-attribute-modeling.component.html',
  styleUrls: ['./binary-attribute-modeling.component.css']
})
export class BinaryAttributeModelingComponent extends BaseAttributeModelingComponent {

  @Input() override attribute: RecproBinaryAttribute = new RecproBinaryAttribute();
}
