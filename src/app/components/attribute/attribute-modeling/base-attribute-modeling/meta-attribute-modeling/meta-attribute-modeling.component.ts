import {Component, Input} from '@angular/core';
import {BaseAttributeModelingComponent} from "../base-attribute-modeling.component";
import {RecproMetaAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproMetaAttribute";

@Component({
  selector: 'app-meta-attribute-modeling',
  templateUrl: './meta-attribute-modeling.component.html',
  styleUrls: ['./meta-attribute-modeling.component.css']
})
export class MetaAttributeModelingComponent  extends BaseAttributeModelingComponent{

  @Input() override attribute: RecproMetaAttribute = new RecproMetaAttribute();

}
