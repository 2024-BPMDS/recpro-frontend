import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaAttributeModelingComponent } from './meta-attribute-modeling.component';

describe('MetaAttributeModelingComponent', () => {
  let component: MetaAttributeModelingComponent;
  let fixture: ComponentFixture<MetaAttributeModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MetaAttributeModelingComponent]
    });
    fixture = TestBed.createComponent(MetaAttributeModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
