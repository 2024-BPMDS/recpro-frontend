import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseAttributeModelingComponent } from './base-attribute-modeling.component';

describe('BaseAttributeModelingComponent', () => {
  let component: BaseAttributeModelingComponent;
  let fixture: ComponentFixture<BaseAttributeModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BaseAttributeModelingComponent]
    });
    fixture = TestBed.createComponent(BaseAttributeModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
