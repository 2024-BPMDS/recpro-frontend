import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextualAttributeModelingComponent } from './textual-attribute-modeling.component';

describe('TextualAttributeModelingComponent', () => {
  let component: TextualAttributeModelingComponent;
  let fixture: ComponentFixture<TextualAttributeModelingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TextualAttributeModelingComponent]
    });
    fixture = TestBed.createComponent(TextualAttributeModelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
