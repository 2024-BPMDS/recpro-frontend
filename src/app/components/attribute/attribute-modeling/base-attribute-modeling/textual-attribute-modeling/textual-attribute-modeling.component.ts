import {Component, Input} from '@angular/core';
import {BaseAttributeModelingComponent} from "../base-attribute-modeling.component";
import {RecproTextualAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproTextualAttribute";

@Component({
  selector: 'app-textual-attribute-modeling',
  templateUrl: './textual-attribute-modeling.component.html',
  styleUrls: ['./textual-attribute-modeling.component.css']
})
export class TextualAttributeModelingComponent extends BaseAttributeModelingComponent{

  @Input() override attribute: RecproTextualAttribute = new RecproTextualAttribute();

}
