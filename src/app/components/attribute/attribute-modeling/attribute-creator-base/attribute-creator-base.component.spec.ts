import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeCreatorBaseComponent } from './attribute-creator-base.component';

describe('AttributeCreatorBaseComponent', () => {
  let component: AttributeCreatorBaseComponent;
  let fixture: ComponentFixture<AttributeCreatorBaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeCreatorBaseComponent]
    });
    fixture = TestBed.createComponent(AttributeCreatorBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
