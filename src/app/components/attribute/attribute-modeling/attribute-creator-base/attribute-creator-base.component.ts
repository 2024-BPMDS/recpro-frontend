import {Component, Input} from '@angular/core';
import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproTextualAttribute} from "../../../../model/RecPro/attributes/modeling/RecproTextualAttribute";
import {RecproAttributeType} from "../../../../model/RecPro/attributes/modeling/RecproAttributeType";
import {RecproBinaryAttribute} from "../../../../model/RecPro/attributes/modeling/RecproBinaryAttribute";
import {RecproMetaAttribute} from "../../../../model/RecPro/attributes/modeling/RecproMetaAttribute";
import {RecproNumericAttribute} from "../../../../model/RecPro/attributes/modeling/RecproNumericAttribute";

@Component({
  selector: 'app-attribute-creator-base',
  templateUrl: './attribute-creator-base.component.html',
  styleUrls: ['./attribute-creator-base.component.css']
})
export class AttributeCreatorBaseComponent {
  @Input() attribute: RecproAttribute = new RecproTextualAttribute();
  typeOptions = Object.values(RecproAttributeType);

  constructor() {
  }

  getTextualAttribute(): RecproTextualAttribute {
    return this.attribute as RecproTextualAttribute;
  }

  getBinaryAttribute(): RecproBinaryAttribute {
    return this.attribute as RecproBinaryAttribute;
  }

  getMetaAttribute(): RecproMetaAttribute {
    return this.attribute as RecproMetaAttribute;
  }

  getNumericAttribute(): RecproNumericAttribute {
    return this.attribute as RecproNumericAttribute;
  }
}
