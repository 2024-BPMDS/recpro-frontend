import {Component, Input} from '@angular/core';
import {RecproAttributeInstanceDto} from "../../../model/RecPro/attributes/RecproAttributeInstanceDto";
import {
  RecproNumericAttributeInstance
} from "../../../model/RecPro/attributes/execution/RecproNumericAttributeInstance";
import {RecproAttributeType} from "../../../model/RecPro/attributes/modeling/RecproAttributeType";
import {RecproNumericAttribute} from "../../../model/RecPro/attributes/modeling/RecproNumericAttribute";
import {RecproBinaryAttribute} from "../../../model/RecPro/attributes/modeling/RecproBinaryAttribute";
import {RecproBinaryAttributeInstance} from "../../../model/RecPro/attributes/execution/RecproBinaryAttributeInstance";
import {RecproTextualAttribute} from "../../../model/RecPro/attributes/modeling/RecproTextualAttribute";
import {
  RecproTextualAttributeInstance
} from "../../../model/RecPro/attributes/execution/RecproTextualAttributeInstance";
import {RecproMetaAttribute} from "../../../model/RecPro/attributes/modeling/RecproMetaAttribute";
import {RecproMetaAttributeInstance} from "../../../model/RecPro/attributes/execution/RecproMetaAttributeInstance";

@Component({
  selector: 'app-attribute-execution',
  templateUrl: './attribute-execution.component.html',
  styleUrls: ['./attribute-execution.component.css']
})
export class AttributeExecutionComponent {

  @Input() attributes: RecproAttributeInstanceDto[] = [];
  @Input() disabled: boolean = true;

  getAttributeType(attribute: RecproAttributeInstanceDto): RecproAttributeType {
    return attribute.attribute.attributeType;
  }

  getRecproNumericAttributeInstance(attribute: RecproAttributeInstanceDto): RecproNumericAttributeInstance {
    return attribute.instance as RecproNumericAttributeInstance;
  }

  getRecproNumericAttribute(attribute: RecproAttributeInstanceDto) {
    return attribute.attribute as RecproNumericAttribute;
  }

  getRecproBinaryAttribute(attribute: RecproAttributeInstanceDto): RecproBinaryAttribute {
    return attribute.attribute as RecproBinaryAttribute;
  }

  getRecproBinaryAttributeInstance(attribute: RecproAttributeInstanceDto): RecproBinaryAttributeInstance {
    return attribute.instance as RecproBinaryAttributeInstance;
  }

  getRecproTextualAttribute(attribute: RecproAttributeInstanceDto): RecproTextualAttribute {
    return attribute.attribute as RecproTextualAttribute;
  }

  getRecproTextualAttributeInstance(attribute: RecproAttributeInstanceDto): RecproTextualAttributeInstance {
    return attribute.instance as RecproTextualAttributeInstance;
  }

  getRecproMetaAttribute(attribute: RecproAttributeInstanceDto): RecproMetaAttribute {
    return attribute.attribute as RecproMetaAttribute;
  }

  getRecproMetaAttributeInstance(attribute: RecproAttributeInstanceDto): RecproMetaAttributeInstance {
    return attribute.instance as RecproMetaAttributeInstance;
  }

}
