import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseAttributeComponent } from './base-attribute.component';

describe('BaseAttributeComponent', () => {
  let component: BaseAttributeComponent;
  let fixture: ComponentFixture<BaseAttributeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BaseAttributeComponent]
    });
    fixture = TestBed.createComponent(BaseAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
