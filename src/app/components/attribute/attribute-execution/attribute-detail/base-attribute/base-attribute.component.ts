import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-base-attribute',
  templateUrl: './base-attribute.component.html',
  styleUrls: ['./base-attribute.component.css']
})
export class BaseAttributeComponent {

  @Input() disabled: boolean = true;

}
