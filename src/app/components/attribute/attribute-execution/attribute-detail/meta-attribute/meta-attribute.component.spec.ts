import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaAttributeComponent } from './meta-attribute.component';

describe('MetaAttributeComponent', () => {
  let component: MetaAttributeComponent;
  let fixture: ComponentFixture<MetaAttributeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MetaAttributeComponent]
    });
    fixture = TestBed.createComponent(MetaAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
