import {Component, Input} from '@angular/core';
import {
  RecproTextualAttributeInstance
} from "../../../../../model/RecPro/attributes/execution/RecproTextualAttributeInstance";
import {RecproTextualAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproTextualAttribute";
import {BaseAttributeComponent} from "../base-attribute/base-attribute.component";

@Component({
  selector: 'app-textual-attribute',
  templateUrl: './textual-attribute.component.html',
  styleUrls: ['./textual-attribute.component.css']
})
export class TextualAttributeComponent extends BaseAttributeComponent{

  @Input() instance: RecproTextualAttributeInstance = new RecproTextualAttributeInstance();
  @Input() attribute: RecproTextualAttribute = new RecproTextualAttribute();

}
