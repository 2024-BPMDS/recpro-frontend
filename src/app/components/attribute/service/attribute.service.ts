import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../util/config.service";
import {Observable} from "rxjs";
import {RecproAttribute} from "../../../model/RecPro/attributes/modeling/RecproAttribute";
import {RecproAttributeInstance} from "../../../model/RecPro/attributes/execution/RecproAttributeInstance";
import {RecproAttributeInstanceDto} from "../../../model/RecPro/attributes/RecproAttributeInstanceDto";
import {
  RecproNumericAttributeInstance
} from "../../../model/RecPro/attributes/execution/RecproNumericAttributeInstance";

@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService
  ) { }

  getAttributesByIds(ids: string[]): Observable<any> {
    const idList = ids.join(',');
    return this.httpClient.get(this.configService.config.getOntologyAttributesByIds() + '?uris=' + idList);
  }

  getAttributesByActivityIds(ids: string[]): Observable<RecproAttribute[]> {
    const idList = ids.join(',');
    return this.httpClient.get<RecproAttribute[]>(this.configService.config.getOntologyAttributesByActivityIds() + '?activityIds=' + idList);
  }

  getAttributesByTaskId(id: number): Observable<RecproAttributeInstance[]> {
    return this.httpClient.get<RecproAttributeInstance[]>(this.configService.config.getAttributeInstanceDtosByTaskId() + '?taskId=' + id);
  }

  saveAttributes(attributeInstances: RecproAttributeInstanceDto[]): Observable<RecproAttributeInstanceDto[]> {
    return this.httpClient.post<RecproAttributeInstanceDto[]>(this.configService.apiConfig.recpro.attribute.attributeExecution.create(), attributeInstances);
  }

  saveAttribute(attributeInstance: RecproNumericAttributeInstance): Observable<RecproNumericAttributeInstance> {
    return this.httpClient.post<RecproNumericAttributeInstance>(this.configService.apiConfig.recpro.attribute.attributeExecution.createAttribute(), attributeInstance);
  }

  // @TODO
  // createAttributeByRecproAttributeInstanceDtos(attributeInstances: RecproAttributeInstanceDto[]): Observable<RecproAttributeInstanceDto[]> {
    // return this.httpClient.post<RecproAttributeInstanceDto[]>(this.configService.apiConfig.recpro.attribute.attributeExecution.createAttributeInstance(), attributeInstances);
  // }


}
