import {Component, Input} from '@angular/core';
import {RecproMetaAttribute} from "../../../../../model/RecPro/attributes/modeling/RecproMetaAttribute";
import {
  RecproFilterMetaAttributeInstance
} from "../../../../bpm/bpm-execution/filter-dialog/model/attribute/RecproFilterMetaAttributeInstance";
import {
  RecproFilterAttributeState
} from "../../../../bpm/bpm-execution/filter-dialog/model/attribute/RecproFilterAttributeState";

@Component({
  selector: 'app-filter-execution-meta-attributes',
  templateUrl: './filter-execution-meta-attributes.component.html',
  styleUrls: ['./filter-execution-meta-attributes.component.css']
})
export class FilterExecutionMetaAttributesComponent {

  @Input() attribute: RecproMetaAttribute = new RecproMetaAttribute();
  @Input() instance: RecproFilterMetaAttributeInstance = new RecproFilterMetaAttributeInstance();


  log() {
    console.log(this.attribute);
    console.log(this.instance);
  }

  changeAttributeStatus(instance: RecproFilterMetaAttributeInstance) {
    switch (this.instance.state) {
      case RecproFilterAttributeState.TRUE: this.instance.state = RecproFilterAttributeState.FALSE;
        break;
      case RecproFilterAttributeState.FALSE: this.instance.state = RecproFilterAttributeState.NEUTRAL;
        break;
      default: this.instance.state = RecproFilterAttributeState.TRUE;
        break;
    }
  }
}
