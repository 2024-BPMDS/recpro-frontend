import { TestBed } from '@angular/core/testing';

import { KnowledgeBasedFilterExecutionService } from './knowledge-based-filter-execution.service';

describe('KnowledgeBasedFilterExecutionService', () => {
  let service: KnowledgeBasedFilterExecutionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KnowledgeBasedFilterExecutionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
