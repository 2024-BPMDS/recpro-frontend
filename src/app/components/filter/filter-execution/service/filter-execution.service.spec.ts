import { TestBed } from '@angular/core/testing';

import { FilterExecutionService } from './filter-execution.service';

describe('FilterExecutionService', () => {
  let service: FilterExecutionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilterExecutionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
