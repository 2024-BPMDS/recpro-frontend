import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {RecproFilter} from "../../../../model/RecPro/filter/RecproFilter";
import {Observable} from "rxjs";
import {RecproFilterInstanceDto} from "../../../bpm/bpm-execution/filter-dialog/model/RecproFilterInstanceDto";
import {RecproTasklistDto} from "../../../../model/BPM/execution/RecproTasklistDto";
import {RecproFilterInstance} from "../../../bpm/bpm-execution/filter-dialog/model/RecproFilterInstance";
import {FilterType} from "../../../../model/RecPro/filter/FilterType";

@Injectable({
  providedIn: 'root'
})
export class KnowledgeBasedFilterExecutionService {

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService,
  ) { }

  createNew(filter: RecproFilter): Observable<RecproFilterInstanceDto> {
    return this.httpClient.post<RecproFilterInstanceDto>(this.configService.apiConfig.recpro.filter.execution.createByFilter(), filter);
  }

  createByFilterId(filterId: string, tasklistId: number, filterType: FilterType): Observable<RecproFilterInstanceDto> {
    return this.httpClient.get<RecproFilterInstanceDto>(this.configService.apiConfig.recpro.filter.execution.createByFilterId(filterId, tasklistId, filterType));
  }

  save(filterInstance: RecproFilterInstance): Observable<RecproTasklistDto> {
    return this.httpClient.post<RecproTasklistDto>(this.configService.apiConfig.recpro.filter.execution.save(), filterInstance);
  }
}
