import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterAttributesComponent } from './filter-attributes/filter-attributes.component';
import { FilterRatingsComponent } from './filter-ratings/filter-ratings.component';
import { FilterBpmElementsComponent } from './filter-bpm-elements/filter-bpm-elements.component';
import {FormsModule} from "@angular/forms";
import {ClarityModule} from "@clr/angular";
import {MaterialModule} from "../../../material/material.module";
import { AddFilterModelingDialogComponent } from './add-filter-modeling-dialog/add-filter-modeling-dialog.component';
import { BaseFilterModelingDetailComponent } from './add-filter-modeling-dialog/base-filter-modeling-detail/base-filter-modeling-detail.component';
import { KnowledgeBasedFilterModelingDetailComponent } from './add-filter-modeling-dialog/knowledge-based-filter-modeling-detail/knowledge-based-filter-modeling-detail.component';
import { ContentBasedFilterModelingDetailComponent } from './add-filter-modeling-dialog/content-based-filter-modeling-detail/content-based-filter-modeling-detail.component';
import { CollaborativeFilterModelingDetailComponent } from './add-filter-modeling-dialog/collaborative-filter-modeling-detail/collaborative-filter-modeling-detail.component';
import { HybridFilterModelingDetailComponent } from './add-filter-modeling-dialog/hybrid-filter-modeling-detail/hybrid-filter-modeling-detail.component';
import { FilterUsersComponent } from './filter-users/filter-users.component';
import {FilterModelingComponent} from "./filter-modeling.component";



@NgModule({
  declarations: [
    FilterUsersComponent,
    FilterRatingsComponent,
    FilterBpmElementsComponent,
    FilterAttributesComponent,
    AddFilterModelingDialogComponent,
    KnowledgeBasedFilterModelingDetailComponent,
    HybridFilterModelingDetailComponent,
    ContentBasedFilterModelingDetailComponent,
    CollaborativeFilterModelingDetailComponent,
    BaseFilterModelingDetailComponent,
    FilterModelingComponent
  ],
  imports: [
    CommonModule,
    ClarityModule,
    MaterialModule,
    FormsModule,
  ]
})
export class FilterModelingModule { }
