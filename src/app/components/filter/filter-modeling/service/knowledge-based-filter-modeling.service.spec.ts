import { TestBed } from '@angular/core/testing';

import { KnowledgeBasedFilterModelingService } from './knowledge-based-filter-modeling.service';

describe('KnowledgeBasedFilterModelingService', () => {
  let service: KnowledgeBasedFilterModelingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KnowledgeBasedFilterModelingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
