import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../../util/config.service";
import {RecproFilter} from "../../../../model/RecPro/filter/RecproFilter";
import {LoaderService} from "../../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class FilterModelingService {

  private filters: Subject<RecproFilter[]> = new Subject<RecproFilter[]>();

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService,
    private loaderService: LoaderService
  ) { }

  getAll(): Observable<RecproFilter[]> {
    this.loaderService.load();
    this.httpClient.get<RecproFilter[]>(this.configService.apiConfig.modeling.filter.getAll()).subscribe(res => {
      this.setFilters(res);
    });
    return this.getFilters();
  }

  getFilters(): Observable<RecproFilter[]> {
    return this.filters.asObservable();
  }

  setFilters(filters: RecproFilter[]) {
    this.filters.next(filters);
    this.loaderService.finish();
  }

  addFilter(filter: RecproFilter) {
    this.loaderService.load();
    this.httpClient.post(this.configService.apiConfig.modeling.filter.create(), filter).subscribe(() => {
      this.getAll();
    });
  }

  delete(filterId: string) {
    this.loaderService.load();
    this.httpClient.delete(this.configService.apiConfig.modeling.filter.delete(filterId)).subscribe(() => {
      this.getAll();
    });
  }
}
