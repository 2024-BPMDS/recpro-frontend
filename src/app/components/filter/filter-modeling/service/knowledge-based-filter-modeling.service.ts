import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {RecproKnowledgeBasedFilter} from "../../../../model/RecPro/filter/RecproKnowledgeBasedFilter";
import {ConfigService} from "../../../../util/config.service";
import {FilterType} from "../../../../model/RecPro/filter/FilterType";

@Injectable({
  providedIn: 'root'
})
export class KnowledgeBasedFilterModelingService {

  private filters: Subject<RecproKnowledgeBasedFilter[]> = new Subject<RecproKnowledgeBasedFilter[]>();

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService
  ) {
    this.filters.next([]);
  }

  getAll(): Observable<RecproKnowledgeBasedFilter[]> {
    this.httpClient.get<RecproKnowledgeBasedFilter[]>(this.configService.apiConfig.modeling.filter.getByType(FilterType.KNOWLEDGE_BASED)).subscribe(res => {
      this.setFilters(res);
    });
    return this.getFilters();
  }

  getFilters(): Observable<RecproKnowledgeBasedFilter[]> {
    return this.filters.asObservable();
  }

  setFilters(filters: RecproKnowledgeBasedFilter[]) {
    this.filters.next(filters);
  }

  addFilter(filter: RecproKnowledgeBasedFilter) {
    this.httpClient.post(this.configService.apiConfig.modeling.filter.create(), filter).subscribe(() => {
      this.getAll();
    });
  }

  delete(filterId: string) {
    this.httpClient.delete(this.configService.apiConfig.modeling.filter.delete(filterId)).subscribe( () => {
      this.getAll();
    });
  }
}
