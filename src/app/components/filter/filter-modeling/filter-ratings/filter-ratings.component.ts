import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Rating} from "../../../../model/RecPro/rating/modelig/Rating";
import {RecproRatingSelected} from "../model/RecproRatingSelected";
import {RatingModelingService} from "../../../rating/rating-modeling/service/rating-modeling.service";

@Component({
  selector: 'app-filter-ratings',
  templateUrl: './filter-ratings.component.html',
  styleUrls: ['./filter-ratings.component.css']
})
export class FilterRatingsComponent {

  @Input() filterRatings: Rating[] = [];
  @Output() filterRatingsChange = new EventEmitter<Rating[]>;

  internalFilterRatings: RecproRatingSelected[] = [];
  availableRatings: RecproRatingSelected[] = [];

  constructor(
    private ratingModelingService: RatingModelingService
  ) {

    this.ratingModelingService.getAll().subscribe(res => {
      this.internalFilterRatings = [];
      this.availableRatings = res.map(rating => new RecproRatingSelected(rating, false));
      this.filterRatings.forEach(rating => {
        const avRating = this.availableRatings.find((a) => a.rating.id === rating.id);
        if (avRating) {
          avRating.selected = true;
        }
      });
    });
  }

  onInternalFilterRatingsChange() {
    this.filterRatings = this.internalFilterRatings.map((filterRating) => filterRating.rating);
    this.filterRatingsChange.emit(this.filterRatings);
  }

}
