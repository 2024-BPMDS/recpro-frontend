import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterRatingsComponent } from './filter-ratings.component';

describe('FilterRatingsComponent', () => {
  let component: FilterRatingsComponent;
  let fixture: ComponentFixture<FilterRatingsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterRatingsComponent]
    });
    fixture = TestBed.createComponent(FilterRatingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
