import {RecproAttribute} from "../../../../model/RecPro/attributes/modeling/RecproAttribute";

export class RecproAttributeSelected {
  attribute: RecproAttribute = new RecproAttribute();
  selected: boolean = false;

  constructor(attribute: RecproAttribute, selected: boolean) {
    this.attribute = attribute;
    this.selected = selected;
  }
}
