import {User} from "../../../../model/User/User";

export class RecproUserSelected {
  user: User = new User();
  selected: boolean = false;

  constructor(user: User, selected: boolean) {
    this.user = user;
    this.selected = selected;
  }
}
