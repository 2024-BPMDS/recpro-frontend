import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RecproElement} from "../../../../model/BPM/modeling/RecproElement";
import {RecproElementService} from "../../../bpm/bpm-modeling/service/recpro-element.service";
import {LoaderService} from "../../../../util/service/loader.service";

@Component({
  selector: 'app-filter-bpm-elements',
  templateUrl: './filter-bpm-elements.component.html',
  styleUrls: ['./filter-bpm-elements.component.css']
})
export class FilterBpmElementsComponent implements OnInit {

  @Input() filterElements: RecproElement[] = [];
  @Output() filterElementsChange = new EventEmitter<RecproElement[]>;
  internalFilterElements: RecproElementSelected[] = [];
  availableElements: RecproElementSelected[] = [];

  constructor(
    private recproElementService: RecproElementService,
    private loaderService: LoaderService
  ) {
  }

  ngOnInit() {
    this.loaderService.load();
    this.recproElementService.getAll().subscribe(res => {
      this.availableElements = res.map(element => new RecproElementSelected(element, false));
      this.filterElements.forEach(element => {
        const availableElement = this.availableElements.find((e) => e.element.id === element.id);
        if (availableElement) {
          availableElement.selected = true;
        }
      });
      this.loaderService.finish();
    });
  }

  onInternalElementsChange() {
    this.filterElements = this.internalFilterElements.map((filterElement) => filterElement.element);
    this.filterElementsChange.emit(this.filterElements);
  }
}

class RecproElementSelected {
  element: RecproElement = new RecproElement();
  selected: boolean = false;
  constructor(element: RecproElement, selected: boolean) {
    this.element = element;
    this.selected = selected;
  }
}
