import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterBpmElementsComponent } from './filter-bpm-elements.component';

describe('FilterBpmElementsComponent', () => {
  let component: FilterBpmElementsComponent;
  let fixture: ComponentFixture<FilterBpmElementsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterBpmElementsComponent]
    });
    fixture = TestBed.createComponent(FilterBpmElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
