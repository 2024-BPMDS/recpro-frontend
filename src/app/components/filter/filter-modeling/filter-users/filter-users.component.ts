import {Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../../../model/User/User";
import {RecproUserSelected} from "../model/RecproUserSelected";
import {UserModelingService} from "../../../user/services/user-modeling.service";

@Component({
  selector: 'app-filter-users',
  templateUrl: './filter-users.component.html',
  styleUrls: ['./filter-users.component.css']
})
export class FilterUsersComponent {

  @Input() filterUsers: User[] = [];
  @Output() filterUsersChange = new EventEmitter<User[]>;

  internalFilterUsers: RecproUserSelected[] = [];
  availableUsers: RecproUserSelected[] = [];

  constructor(
    private userModelingService: UserModelingService
  ) {
    this.userModelingService.getAll().subscribe(res => {
      this.internalFilterUsers = [];
      this.availableUsers = res.map(user => new RecproUserSelected(user, false));
      this.filterUsers.forEach(user => {
        const avUser = this.availableUsers.find((a) => a.user.id === user.id);
        if (avUser) {
          avUser.selected = true;
        }
      });
    });
  }

  onInternalFilterUsersChange() {
    this.filterUsers = this.internalFilterUsers.map((filterUser) => filterUser.user);
    this.filterUsersChange.emit(this.filterUsers);
  }

}
