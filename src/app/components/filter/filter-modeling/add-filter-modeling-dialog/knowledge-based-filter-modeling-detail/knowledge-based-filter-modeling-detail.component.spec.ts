import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KnowledgeBasedFilterModelingDetailComponent } from './knowledge-based-filter-modeling-detail.component';

describe('KnowledgeBasedFilterModelingDetailComponent', () => {
  let component: KnowledgeBasedFilterModelingDetailComponent;
  let fixture: ComponentFixture<KnowledgeBasedFilterModelingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [KnowledgeBasedFilterModelingDetailComponent]
    });
    fixture = TestBed.createComponent(KnowledgeBasedFilterModelingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
