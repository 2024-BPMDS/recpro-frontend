import {Component, Inject} from '@angular/core';
import {RecproFilter} from "../../../../model/RecPro/filter/RecproFilter";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FilterModelingService} from "../service/filter-modeling.service";
import {RecproKnowledgeBasedFilter} from "../../../../model/RecPro/filter/RecproKnowledgeBasedFilter";
import {RecproBaseFilter} from "../../../../model/RecPro/filter/RecproBaseFilter";
import {RecproContentBasedFilter} from "../../../../model/RecPro/filter/RecproContentBasedFilter";
import {RecproCollaborativeFilter} from "../../../../model/RecPro/filter/RecproCollaborativeFilter";
import {RecproHybridFilter} from "../../../../model/RecPro/filter/RecproHybridFilter";
import {FilterType} from "../../../../model/RecPro/filter/FilterType";

@Component({
  selector: 'app-add-filter-modeling-dialog',
  templateUrl: './add-filter-modeling-dialog.component.html',
  styleUrls: ['./add-filter-modeling-dialog.component.css']
})
export class AddFilterModelingDialogComponent {

  filter: RecproFilter = new RecproFilter();
  typeOptions = Object.values(FilterType);

  constructor(
    private dialogRef: MatDialogRef<AddFilterModelingDialogComponent>,
    private service: FilterModelingService,
    @Inject(MAT_DIALOG_DATA) public filterData: RecproFilter
  ) {
    console.log(this.filterData);
    if (this.filterData) {
      this.filter = this.filterData;
    } else {
      this.createNewFilter(this.filter.filterType.valueOf());
    }
  }

  createNewFilter(filterType: string) {
    switch (filterType) {
      case FilterType.KNOWLEDGE_BASED:
        this.filter = new RecproKnowledgeBasedFilter();
        break;
      case FilterType.BASE:
        this.filter = new RecproBaseFilter();
        break;
      case FilterType.HYBRID:
        this.filter = new RecproHybridFilter();
        break;
      case FilterType.COLLABORATIVE:
        this.filter = new RecproCollaborativeFilter();
        break;
      case FilterType.CONTENT_BASED:
        this.filter = new RecproContentBasedFilter();
        break;
      default: this.filter = new RecproFilter();
    }
  }

  save() {
    this.service.addFilter(this.filter);
    this.dialogRef.close();
  }

  cancel() {
    this.service.getAll();
    this.dialogRef.close();
  }

  getKnowledgeBasedFilter(): RecproKnowledgeBasedFilter {
    return this.filter as RecproKnowledgeBasedFilter;
  }

  getBaseFilter(): RecproBaseFilter {
    return this.filter as RecproBaseFilter;
  }

  getContentBasedFilter(): RecproContentBasedFilter {
    return this.filter as RecproContentBasedFilter;
  }

  getCollaborativeFilter(): RecproCollaborativeFilter {
    return this.filter as RecproCollaborativeFilter;
  }

  getHybridFilter(): RecproHybridFilter {
    return this.filter as RecproHybridFilter;
  }

  log() {
    console.log(this.filter);
  }

}
