import {Component, Input} from '@angular/core';
import {RecproContentBasedFilter} from "../../../../../model/RecPro/filter/RecproContentBasedFilter";

@Component({
  selector: 'app-content-based-filter-modeling-detail',
  templateUrl: './content-based-filter-modeling-detail.component.html',
  styleUrls: ['./content-based-filter-modeling-detail.component.css']
})
export class ContentBasedFilterModelingDetailComponent {

  @Input() filter: RecproContentBasedFilter = new RecproContentBasedFilter();

}
