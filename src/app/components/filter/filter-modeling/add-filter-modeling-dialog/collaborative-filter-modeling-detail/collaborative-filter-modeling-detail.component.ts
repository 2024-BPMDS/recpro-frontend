import {Component, Input} from '@angular/core';
import {RecproCollaborativeFilter} from "../../../../../model/RecPro/filter/RecproCollaborativeFilter";

@Component({
  selector: 'app-collaborative-filter-modeling-detail',
  templateUrl: './collaborative-filter-modeling-detail.component.html',
  styleUrls: ['./collaborative-filter-modeling-detail.component.css']
})
export class CollaborativeFilterModelingDetailComponent {

  @Input() filter: RecproCollaborativeFilter = new RecproCollaborativeFilter();
}
