import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborativeFilterModelingDetailComponent } from './collaborative-filter-modeling-detail.component';

describe('CollaborativeFilterModelingDetailComponent', () => {
  let component: CollaborativeFilterModelingDetailComponent;
  let fixture: ComponentFixture<CollaborativeFilterModelingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CollaborativeFilterModelingDetailComponent]
    });
    fixture = TestBed.createComponent(CollaborativeFilterModelingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
