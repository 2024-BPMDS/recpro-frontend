import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFilterModelingDetailComponent } from './base-filter-modeling-detail.component';

describe('BaseFilterModelingDetailComponent', () => {
  let component: BaseFilterModelingDetailComponent;
  let fixture: ComponentFixture<BaseFilterModelingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BaseFilterModelingDetailComponent]
    });
    fixture = TestBed.createComponent(BaseFilterModelingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
