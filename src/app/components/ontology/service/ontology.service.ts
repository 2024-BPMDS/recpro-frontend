import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../../../util/config.service";
import {LoaderService} from "../../../util/service/loader.service";

@Injectable({
  providedIn: 'root'
})
export class OntologyService {

  constructor(
    private http: HttpClient,
    private config: ConfigService,
    private loaderService: LoaderService
  ) { }

  clearOntology() {
    this.loaderService.load();
    this.http.get(this.config.apiConfig.modeling.ontology.clear()).subscribe(() => this.loaderService.finish());
  }
}
