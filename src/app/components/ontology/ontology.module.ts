import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OntologyBaseComponent } from './ontology-base/ontology-base.component';



@NgModule({
  declarations: [
    OntologyBaseComponent
  ],
  imports: [
    CommonModule
  ]
})
export class OntologyModule { }
